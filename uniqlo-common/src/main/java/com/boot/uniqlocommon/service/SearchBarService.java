package com.boot.uniqlocommon.service;

import com.boot.uniqlocommon.vo.GoodsVo;

import java.util.List;

/**
 * Created by Charon on 2020/7/14/014
 */
public interface SearchBarService {

    List<GoodsVo> queryGoods(String goodsName, int pageNo);

    long goodsTotalCount();
}
