package com.boot.uniqlocommon.service;

import com.boot.uniqlocommon.vo.ConsultVo;

/**
 * Created by zyf 2020/7/15
 */
public interface ConsultService {

    public Integer addConsult(ConsultVo consultVo);
}
