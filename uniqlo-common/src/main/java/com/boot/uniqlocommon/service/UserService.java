package com.boot.uniqlocommon.service;

import com.boot.uniqlocommon.vo.UsersVo;

/**
 * Shenhao 2020/7/14
 **/
public interface UserService {
    //数据库查询用户登录状态
    public UsersVo queryUsers(String Id);

}
