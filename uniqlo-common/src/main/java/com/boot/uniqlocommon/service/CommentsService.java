package com.boot.uniqlocommon.service;


import com.boot.uniqlocommon.param.CommentsParam;
import com.boot.uniqlocommon.vo.CommentsVo;

import java.util.List;

/**
 * Shenhao 2020/7/15
 **/
public interface CommentsService {

    //通过订单id查询是否支付接口
    List<CommentsVo> queryOrders(String id, int pageNo);

    //实现增加评论的方法
    Integer addcomments(CommentsParam commentsParam);

    long queryOrdersTotalCount(String gid);
}
