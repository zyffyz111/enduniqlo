package com.boot.uniqlocommon.service;

import com.boot.uniqlocommon.vo.CommentsVo;

/**
 * Created by zyf 2020/7/16
 */
public interface CommentService {

    public Integer addComment(CommentsVo commentsVo);
}
