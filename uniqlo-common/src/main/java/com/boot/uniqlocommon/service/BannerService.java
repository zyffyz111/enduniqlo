package com.boot.uniqlocommon.service;

import com.boot.uniqlocommon.vo.BannerVo;

import java.util.List;

/**
 * Shenhao 2020/7/13
 **/
public interface BannerService {
    //抽象方法存（增加），接口
    public Integer addBanner(BannerVo bannerVo);

    //抽象方法（查询），接口
    public List<BannerVo> queryBanner();
}
