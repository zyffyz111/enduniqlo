package com.boot.uniqlocommon.service;

import com.boot.uniqlocommon.vo.ChinaVo;

import java.util.List;

/**
 * Create by maodihui on 2020/7/16
 */
public interface ChinaService {

    ChinaVo getByName(String name);

    public List<ChinaVo> getInfo(Integer pid);

    public ChinaVo getByPrimaryId(Integer id);
}
