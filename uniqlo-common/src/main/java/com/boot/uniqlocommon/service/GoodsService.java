package com.boot.uniqlocommon.service;

import com.boot.uniqlocommon.utils.PageUtils;
import com.boot.uniqlocommon.vo.GoodsVo;
import com.boot.uniqlocommon.vo.TypeVo;

import java.util.List;

/**
 * Created by Charon on 2020/7/15/015
 */
public interface GoodsService {

    boolean cutCountAndUpdateOrders(String orderId);

    GoodsVo getGoodsInfoById(String id);

    public List<TypeVo> getTopType();

    public List<TypeVo> getTypeByParentId(Integer id);

    public PageUtils getGoodsByTypeId(Integer typeId, Integer pageno);
}
