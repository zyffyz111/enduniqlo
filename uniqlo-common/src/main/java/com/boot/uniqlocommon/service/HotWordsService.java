package com.boot.uniqlocommon.service;

import com.boot.uniqlocommon.vo.HotWordsVo;

import java.util.List;

/**
 * Created by Charon on 2020/7/14/014
 */
public interface HotWordsService {

    List<HotWordsVo> hotWords();

    Integer addCount(String goodsName);
}
