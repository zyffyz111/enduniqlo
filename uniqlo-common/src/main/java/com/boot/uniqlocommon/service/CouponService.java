package com.boot.uniqlocommon.service;

import com.boot.uniqlocommon.vo.CouponUserVo;
import com.boot.uniqlocommon.vo.CouponVo;

import java.util.List;

/**
 * Created by Charon on 2020/7/15/015
 */
public interface CouponService {

    String getCoupon(String uId, String id);

    List<CouponVo> couponListByGid(String gid);

    CouponVo getById(String id);

    boolean isExistAndIsUsed(String couponUserId, String uid);

    boolean filter(double total_fee, String couponUserId);

    CouponUserVo getByCouponUserId(String couponUserId);

    Integer updateCouponUserStatus(String couponUserID);

}
