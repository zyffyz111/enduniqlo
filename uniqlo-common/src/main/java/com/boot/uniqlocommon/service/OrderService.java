package com.boot.uniqlocommon.service;

import com.boot.uniqlocommon.vo.OrdersVo;

import java.util.List;

/**
 * Created by Charon on 2020/7/15/015
 */
public interface OrderService {

    int addNotPayOrders(OrdersVo ordersVo);

    boolean checkNum(OrdersVo ordersVo);

    public List<OrdersVo> getOrdersByGidAndUid(String gid, String uid);

}
