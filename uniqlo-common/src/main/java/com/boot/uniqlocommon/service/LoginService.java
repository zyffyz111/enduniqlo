package com.boot.uniqlocommon.service;

import com.boot.uniqlocommon.vo.UsersVo;

/**
 * Create by maodihui on 2020/7/14
 */
public interface LoginService {

    //查找手机号
    Integer findPhone(String phone);

    //手机注册
    Integer create(String phone);

    //手机登录
    UsersVo search(String phone);

    //绑定手机
    Integer bindPhone(String openid, String phone);

    //微信登录
    UsersVo search2(String openid);

    //绑定微信
    Integer wxLogin(String phone, String openid, String nickName, String sex, String address, String headimg);

    //微信注册
    Integer registered(String id, String openid, String nickName, String sex, String address, String headimg);

}
