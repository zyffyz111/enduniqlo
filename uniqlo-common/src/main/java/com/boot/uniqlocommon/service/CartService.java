package com.boot.uniqlocommon.service;

import com.boot.uniqlocommon.vo.CartVo;
import com.boot.uniqlocommon.vo.IpGoodsVo;

import java.util.List;

/**
 * Create by maodihui on 2020/7/15
 */


public interface CartService {

    //查找当地库存
    Integer queryStock(String gid, String area);

    //加入IP购物车
    Integer addIpCart(IpGoodsVo ipGoodsVo);

    //加入购物车
    Integer addCart(CartVo cartVo);

    //删除IP购物车
    Integer deleteIp(String gid);

    //删除Cart购物车
    Integer deleteCart(String gid);

    //转移购物车
    List<IpGoodsVo> queryAllIp(String ip);

    //插入Cart
    Integer insertCart(String uid, IpGoodsVo ipGoodsVo);


}
