package com.boot.uniqlocommon.service;


import com.boot.uniqlocommon.vo.GoodsVo;

import java.util.List;

/**
 * Created by Charon on 2020/7/14/014
 */
public interface CollectService {

    List<GoodsVo> collectGoodsList(String uId, int pageNo);

    long collectGoodsListTotalCount(String uId);

    void addToCollection(String uId, String gId);
}
