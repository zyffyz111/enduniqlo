package com.boot.uniqlocommon.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.boot.uniqlocommon.utils.HttpClientUtils;
import com.boot.uniqlocommon.vo.ChinaVo;

import java.io.IOException;
import java.util.Map;


public class IPUtils {


    /*public static String getV4IP() {
        String ip = "";
        String chinaz = "http://ip.chinaz.com";
        StringBuilder inputLine = new StringBuilder();
        String read = "";
        URL url = null;
        HttpURLConnection urlConnection = null;
        BufferedReader in = null;
        try {
            url = new URL(chinaz);
            urlConnection = (HttpURLConnection) url.openConnection();
            in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
            while ((read = in.readLine()) != null) {
                inputLine.append(read + "\r\n");
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        Pattern p = Pattern.compile("\\<dd class\\=\"fz24\">(.*?)\\<\\/dd>");
        Matcher m = p.matcher(inputLine.toString());
        if (m.find()) {
            String ipstr = m.group(1);
            ip = ipstr;
        }
        return ip;
    }*/

    public static String getV4IP() {
        try {
            String url = "http://pv.sohu.com/cityjson?ie=utf-8";
            String str = HttpClientUtils.doGet(url);
            String[] split = str.split("=");
            String json = split[1].replaceAll(";", "").trim();
            JSONObject jsonObject = JSON.parseObject(json);
            return jsonObject.getString("cip");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
