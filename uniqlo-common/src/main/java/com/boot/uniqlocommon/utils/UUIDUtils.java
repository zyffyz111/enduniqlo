package com.boot.uniqlocommon.utils;

import java.util.UUID;

/**
 * Created by xueqijun on 2020/7/9.
 */
public class UUIDUtils {
    public static String getUUIDStr(int len) {
        return UUID.randomUUID().toString().replaceAll("-", "").substring(0, len);
    }
}
