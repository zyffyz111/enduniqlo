package com.boot.uniqlocommon.utils;

import lombok.Data;

import java.io.Serializable;
import java.util.Collection;

/**
 * Created by boot on 2020/6/17.
 */
@Data
public class PageUtils<T> implements Serializable {
    private int currentPage;//当前页
    private int pageNo = 1;//页码
    private Integer pageSize = 15;//每页条数
    private long totalPage;//总页数
    private long totalCount;//总条数
    private Collection<T> currentList;


    public int getPageNo() {
        return (pageNo - 1) * pageSize;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public long getTotalPage() {
        return totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;
    }

    public void setTotalPage(long totalPage) {
        this.totalPage = totalPage;
    }
}
