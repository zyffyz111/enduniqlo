package com.boot.uniqlocommon.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by xueqijun on 2020/7/9.
 */
@Data
@ApiModel
public class LoginUserVo implements Serializable {

    @ApiModelProperty(hidden = true)
    private String id;
    @ApiModelProperty(hidden = true)
    private String phone;
    @ApiModelProperty(hidden = true)
    private String openid;
}
