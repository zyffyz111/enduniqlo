package com.boot.uniqlocommon.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table comments
 *
 * @mbg.generated do_not_delete_during_merge
 */
@Data
public class CommentsParam {

    @ApiModelProperty(value = "用户id")
    private String uid;

    @ApiModelProperty(value = "商品id")
    private String gid;

    @ApiModelProperty(value = "评论内容")
    private String content;

}