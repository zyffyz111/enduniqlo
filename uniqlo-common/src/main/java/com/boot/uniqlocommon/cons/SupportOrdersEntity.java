package com.boot.uniqlocommon.cons;

/**
 * Created by xueqijun on 2020/7/9.
 */

public class SupportOrdersEntity {

    public final static String SEND_QUEUE_ORDER_NAME = "send_queue_order_name";

    public final static String GOODS_COUNT_NAMESPACE_LOCK = "goods_count_namespace_lock:";

    public final static String GOODS_COUNT_NAMESPACE = "goods_count_namespace:";

    public final static String WX_PAY_NOTIFY_NAMESPACE = "wx_pay_notify_namespace:";

    public final static String BUY_GET_COUPON_SUCCESS = "领取优惠券成功！";

    public final static String BUY_GET_COUPON_FAIL = "领取优惠券失败！";

    public final static String UPDATE_COUPON_SUCCESS = "修改优惠券成功！";

    public final static String UPDATE_COUPON_FAIL = "修改优惠券失败！";

    public final static String ADD_TO_COLLECTION_SUCCESS = "添加到收藏夹成功！";

    public final static String ADD_TO_COLLECTION_FAIL = "添加到收藏夹失败！";

    public static final String ORDER_NOT_PAY = "order_not_pay:";

    public static final String USER_PRE_IP = "user_pre_ip:";

}
