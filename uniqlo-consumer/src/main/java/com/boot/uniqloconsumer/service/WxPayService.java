package com.boot.uniqloconsumer.service;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.boot.uniqlocommon.service.CouponService;
import com.boot.uniqlocommon.service.GoodsService;
import com.boot.uniqlocommon.utils.HttpClientUtils;
import com.boot.uniqlocommon.utils.UUIDUtils;
import com.boot.uniqlocommon.vo.CouponUserVo;
import com.boot.uniqlocommon.vo.CouponVo;
import com.boot.uniqloconsumer.config.wxConfig.WxPayConfig;
import com.boot.uniqlocommon.vo.GoodsVo;
import com.boot.uniqlocommon.vo.OrdersVo;
import com.boot.uniqloconsumer.utils.WXPayUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by Charon on 2020/7/15/015
 */
@Component
public class WxPayService {

    @Autowired
    private WxPayConfig wxPayConfig;
    @Reference
    private GoodsService goodsService;
    @Reference
    private CouponService couponService;

    /**
     * 生成订单
     *
     * @param gId    商品id
     * @param number 商品数量
     * @param uId    用户id
     * @return
     */
    public OrdersVo isOrder(String gId, int number, String uId) {
        GoodsVo goodsVo = goodsService.getGoodsInfoById(gId);
        OrdersVo ordersVo = new OrdersVo();
        ordersVo.setId(UUIDUtils.getUUIDStr(20));
        ordersVo.setgId(goodsVo.getId());
        ordersVo.setNumber(number);
        ordersVo.setPrice(goodsVo.getPrice() * number);
        ordersVo.setuId(uId);
        return ordersVo;
    }

    public OrdersVo isOrder(String gId, int number, String uId, String couponUserId) {
        CouponUserVo couponUserVo = couponService.getByCouponUserId(couponUserId);
        CouponVo couponVo = couponService.getById(couponUserVo.getCouponId());
        GoodsVo goodsVo = goodsService.getGoodsInfoById(gId);
        OrdersVo ordersVo = new OrdersVo();
        ordersVo.setId(UUIDUtils.getUUIDStr(20));
        ordersVo.setgId(goodsVo.getId());
        ordersVo.setNumber(number);
        ordersVo.setPrice(goodsVo.getPrice() * number - couponVo.getMoney());
        ordersVo.setuId(uId);
        ordersVo.setCouponuserid(couponUserId);
        return ordersVo;
    }

    /**
     * 调用微信支付统一下单
     *
     * @param ordersVo
     * @return
     */
    public String unifiedOrders(OrdersVo ordersVo) throws Exception {

        SortedMap<String, String> param = new TreeMap<>();
        param.put("appid", wxPayConfig.getAppId());
        param.put("mch_id", wxPayConfig.getMchId());
        param.put("nonce_str", UUIDUtils.getUUIDStr(32));
        param.put("body", "test");
        param.put("out_trade_no", ordersVo.getId());
        param.put("total_fee", "1");
        param.put("spbill_create_ip", "192.168.75.1");
        param.put("notify_url", wxPayConfig.getNotifyUrl());
        param.put("trade_type", "NATIVE");
        String s = wxPayConfig.getKey();
        param.put("sign", WXPayUtil.generateSignature(param, s));
        String payXml = WXPayUtil.mapToXml(param);
        String result = HttpClientUtils.doPost(wxPayConfig.getUnifiedOrderUrl(), payXml, 5000);
        if (StringUtils.isNotEmpty(result)) {
            Map<String, String> resultMap = WXPayUtil.xmlToMap(result);
            if (checkResult(resultMap)) {
                return resultMap.get("code_url");
            }
        }
        return null;
    }

    /**
     * @param resultMap
     * @return
     */
    public boolean checkResult(Map<String, String> resultMap) {
        return "SUCCESS".equals(resultMap.get("return_code")) && "SUCCESS".equals(resultMap.get("result_code"));
    }
}
