package com.boot.uniqloconsumer.utils;

import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

import javax.jms.Destination;

/**
 * Created by zyf 2020/7/8
 */
@Component
public class ActiveUtils {

    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    //点对点模式
    public void sendQueueMsg(String name, Object msg) {
        Destination destination = new ActiveMQQueue(name);
        jmsMessagingTemplate.convertAndSend(destination, msg);
    }

    //订阅模式
    public void sendTopicMsg(String name, Object msg) {
        Destination destination = new ActiveMQTopic(name);
        jmsMessagingTemplate.convertAndSend(destination, msg);
    }
}
