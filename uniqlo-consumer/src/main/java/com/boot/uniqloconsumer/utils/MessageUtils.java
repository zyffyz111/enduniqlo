package com.boot.uniqloconsumer.utils;


import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.boot.uniqlocommon.utils.RedisUtils;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * Created by zyf 2020/7/14
 */
@Data
@Component
@ConfigurationProperties(prefix = "phone")
public class MessageUtils {

    @Autowired
    private RedisUtils redisUtils;

    private String regionId;
    private String accessKeyId;
    private String secret;
    private String time;
    private String uri;
    private String signName;
    private String templateCode;
    private String sms;

    public String send(String phone) {
        DefaultProfile profile = DefaultProfile.getProfile(getRegionId(), getAccessKeyId(), getSecret());
        IAcsClient client = new DefaultAcsClient(profile);
        Random random = new Random();
        String code = String.valueOf(random.nextInt(9999));
        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain(getUri());
        request.setSysVersion(getTime());
        request.setSysAction(getSms());
        request.putQueryParameter("RegionId", getRegionId());
        request.putQueryParameter("PhoneNumbers", phone);
        request.putQueryParameter("SignName", getSignName());
        request.putQueryParameter("TemplateCode", getTemplateCode());
        request.putQueryParameter("TemplateParam", "{\"code\":\"" + code + "\"}");
        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
            redisUtils.set("yzm", code, 3000);
            return code;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}

