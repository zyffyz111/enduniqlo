package com.boot.uniqloconsumer.controller.zyw;

import com.alibaba.dubbo.config.annotation.Reference;
import com.boot.uniqlocommon.service.ChinaService;
import com.boot.uniqlocommon.service.CouponService;
import com.boot.uniqlocommon.service.GoodsService;
import com.boot.uniqlocommon.service.OrderService;
import com.boot.uniqlocommon.utils.RedisUtils;
import com.boot.uniqlocommon.vo.LoginUserVo;
import com.boot.uniqloconsumer.config.interAPI.CurrentUser;
import com.boot.uniqloconsumer.config.interAPI.LoginRequired;
import com.boot.uniqloconsumer.config.wxConfig.WxPayConfig;
import com.boot.uniqloconsumer.utils.WXPayUtil;
import com.boot.uniqlocommon.vo.OrdersVo;
import com.boot.uniqloconsumer.service.WxPayService;
import com.boot.uniqloconsumer.utils.ActiveUtils;
import com.boot.uniqloconsumer.utils.WeatherUtils;
import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

import static com.boot.uniqlocommon.cons.SupportOrdersEntity.*;

/**
 * Created by Charon on 2020/7/15/015
 */
@Api(tags = "微信支付相关")
@RestController
@RequestMapping(value = "/wxPay")
public class WxPayController {

    @Autowired
    private ActiveUtils activeUtils;
    @Autowired
    private WxPayService wxPayService;
    @Reference
    private OrderService orderService;
    @Reference
    private GoodsService goodsService;
    @Reference
    private ChinaService chinaService;
    @Autowired
    private WxPayConfig wxPayConfig;
    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private WeatherUtils weatherUtils;
    @Reference
    private CouponService couponService;

    @JmsListener(destination = SEND_QUEUE_ORDER_NAME)
    public void addOrder(OrdersVo ordersVo) {
        // 在什么情况下才能够生产未支付订单呢？？？库存够，已解锁状态下
        if (orderService.checkNum(ordersVo)) {
            redisUtils.set(ORDER_NOT_PAY + ordersVo.getId(), "1", 300);
            orderService.addNotPayOrders(ordersVo);
        }
    }

    @ApiOperation(value = "组装订单, 调用支付接口")
    @LoginRequired
    @GetMapping(value = "/toPay")
    public String WxPay(@RequestParam("商品id") String gId,
                        @RequestParam("商品数量") int number,
                        @RequestParam(value = "优惠券id", required = false) String couponUserId,
                        @ApiParam(hidden = true) @CurrentUser LoginUserVo loginUserVo) throws Exception {
        if (ObjectUtils.isEmpty(loginUserVo)) {
            return "请登陆后进行操作";
        }
        OrdersVo ordersVo = null;
        if (null == couponUserId) {
            ordersVo = wxPayService.isOrder(gId, number, loginUserVo.getId());
        } else {
            //TODO 判断优惠券使用存在和使用状态
            if (!couponService.isExistAndIsUsed(couponUserId, loginUserVo.getId())) {
                return "优惠券已使用或者不存在或者过期";
            }
            //TODO 判断优惠券条件
            ordersVo = wxPayService.isOrder(gId, number, loginUserVo.getId(), couponUserId);
            if (!couponService.filter(ordersVo.getPrice(), couponUserId)) {
                return "优惠券条件不符合,请重新下单";
            }

        }

        activeUtils.sendQueueMsg(SEND_QUEUE_ORDER_NAME, ordersVo);
        Thread.sleep(2000);
        if (null == redisUtils.get(ORDER_NOT_PAY + ordersVo.getId())) {
            return "订单不存在，请重新下单";
        }
        return wxPayService.unifiedOrders(ordersVo);
    }

    /**
     * wxPay 支付结果通知
     *
     * @param request
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "微信支付回调函数", hidden = true)
    @RequestMapping(value = "/wxPayResult")
    public String wxPayResult(HttpServletRequest request) throws Exception {
        InputStream inputStream = request.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        StringBuffer sb = new StringBuffer();
        String line;
        while (null != (line = bufferedReader.readLine())) {
            sb.append(line);
        }
        bufferedReader.close();
        inputStream.close();
        Map<String, String> resultMap = WXPayUtil.xmlToMap(sb.toString());

        //签名校验
        if (WXPayUtil.checkSign(resultMap, wxPayConfig.getKey())) {
            String orderId = resultMap.get("out_trade_no");
            // 如果支付成功 减库存 修改订单状态
            goodsService.cutCountAndUpdateOrders(orderId);
            redisUtils.del(ORDER_NOT_PAY + orderId);
            //redisUtils.set(WX_PAY_NOTIFY_NAMESPACE + orderId, new String[0]);
            Map<String, String> wxMap = Maps.newHashMap();
            wxMap.put("return_code", "SUCCESS");
            wxMap.put("return_msg", "OK");
            return WXPayUtil.mapToXml(wxMap);
        }

        return null;
    }
}
