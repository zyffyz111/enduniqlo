package com.boot.uniqloconsumer.controller.shenhao;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.boot.uniqlocommon.service.UserService;
import com.boot.uniqlocommon.utils.RedisUtils;
import com.boot.uniqlocommon.utils.ReturnResult;
import com.boot.uniqlocommon.utils.ReturnResultUtils;
import com.boot.uniqlocommon.vo.LoginUserVo;
import com.boot.uniqlocommon.vo.UsersVo;
import com.boot.uniqloconsumer.config.interAPI.CurrentUser;
import com.boot.uniqloconsumer.config.interAPI.LoginRequired;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Shenhao 2020/7/14
 **/
//controller两个注解

@Api(tags = "用户个人信息")
@RestController
@RequestMapping(value = "UserController")
public class UsersController {
    //调用sercice,再调用mapper
    @Reference
    private UserService userService;

    @Autowired
    private RedisUtils redisUtils;

    @GetMapping(value = "useInfo")
    @LoginRequired
    @ApiOperation("获取用户信息")
    public ReturnResult<UsersVo> userInfo(@CurrentUser LoginUserVo loginUserVo) {
        UsersVo usersVo = userService.queryUsers(loginUserVo.getId());
        if (ObjectUtils.isEmpty(usersVo)) {
            return ReturnResultUtils.returnFail(101, "获取用户信息失败");
        }
        return ReturnResultUtils.returnSuccess(usersVo);
    }

}
