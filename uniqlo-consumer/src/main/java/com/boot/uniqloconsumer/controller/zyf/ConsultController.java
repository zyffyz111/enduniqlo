package com.boot.uniqloconsumer.controller.zyf;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.boot.uniqlocommon.service.ConsultService;
import com.boot.uniqlocommon.utils.RedisUtils;
import com.boot.uniqlocommon.utils.ReturnResult;
import com.boot.uniqlocommon.utils.ReturnResultUtils;
import com.boot.uniqlocommon.vo.ConsultVo;
import com.boot.uniqlocommon.vo.LoginUserVo;
import com.boot.uniqloconsumer.config.interAPI.CurrentUser;
import com.boot.uniqloconsumer.config.interAPI.LoginRequired;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by zyf 2020/7/15
 */
@Api(tags = "咨询功能")
@RestController
@RequestMapping(value = "/consult")
public class ConsultController {

    @Reference
    private ConsultService consultService;

    @Autowired
    private RedisUtils redisUtils;

    @ApiOperation("添加咨询")
    @LoginRequired
    @PostMapping(value = "/add")
    public ReturnResult addConsult(@Validated ConsultVo consultVo, @ApiParam(hidden = true) @CurrentUser LoginUserVo loginUserVo) {
        if (ObjectUtils.isEmpty(loginUserVo)) {
            return ReturnResultUtils.returnCodeAndMsg(601, "请登陆后进行操作");
        }
        try {
            consultVo.setUid(loginUserVo.getId());
            consultService.addConsult(consultVo);
        } catch (Exception e) {
            return ReturnResultUtils.returnCodeAndMsg(602, "咨询错误");
        }
        return ReturnResultUtils.returnSuccess();
    }

}
