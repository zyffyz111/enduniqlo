package com.boot.uniqloconsumer.controller.mdh;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.boot.uniqlocommon.service.CartService;
import com.boot.uniqlocommon.service.ChinaService;
import com.boot.uniqlocommon.utils.RedisUtils;
import com.boot.uniqlocommon.utils.ReturnResult;
import com.boot.uniqlocommon.utils.ReturnResultUtils;
import com.boot.uniqlocommon.vo.CartVo;
import com.boot.uniqlocommon.vo.ChinaVo;
import com.boot.uniqlocommon.vo.IpGoodsVo;
import com.boot.uniqlocommon.vo.LoginUserVo;
import com.boot.uniqloconsumer.config.interAPI.CurrentUser;
import com.boot.uniqloconsumer.config.interAPI.LoginRequired;
import com.boot.uniqlocommon.utils.IPUtils;
import com.boot.uniqloconsumer.utils.WeatherUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.boot.uniqlocommon.cons.SupportOrdersEntity.USER_PRE_IP;

/**
 * Create by maodihui on 2020/7/15
 */
@Api(tags = "添加购物车")
@RestController
@RequestMapping(value = "/cart")
public class CartController {


    @Autowired
    private WeatherUtils weatherUtils;
    @Reference
    private CartService cartService;
    @Reference
    private ChinaService chinaService;
    @Autowired
    private RedisUtils redisUtils;

    @ApiOperation("未登录状态下加入购物车")
    @PostMapping(value = "/addIpCart")
    public ReturnResult addIpCart(@ApiParam("商品id") String gid, @ApiParam("商品数量") int number) {

        String ip = IPUtils.getV4IP();
        Map<String, String> map = weatherUtils.getWeatherInfoByIp(ip);
        ChinaVo chinaVo = chinaService.getByName(map.get("city"));

        int stock = cartService.queryStock(gid, chinaVo.getName());
        if (stock == -1) {
            return ReturnResultUtils.returnFail(400, "乜有该货物啊！！");
        }
        if (number > stock) {
            return ReturnResultUtils.returnFail(400, "货物不足");
        }
        IpGoodsVo vo = new IpGoodsVo();
        vo.setGid(gid);
        vo.setIp(ip);
        vo.setNumber(number);
        cartService.addIpCart(vo);
        return ReturnResultUtils.returnSuccess(vo);
    }

    @LoginRequired
    @ApiOperation("登录状态下加入购物车")
    @PostMapping(value = "/addCart")
    public ReturnResult addCart(@ApiParam("商品id") @RequestParam("gid") String gid, @ApiParam("商品数量") @RequestParam("number") Integer number, @ApiParam(hidden = true) @CurrentUser LoginUserVo loginUserVo) {

        String ip = IPUtils.getV4IP();
        List<IpGoodsVo> vos = cartService.queryAllIp(ip);
        vos.forEach(vo -> {
            cartService.insertCart(loginUserVo.getId(), vo);
        });

        if (ObjectUtils.isEmpty(loginUserVo)) {
            return ReturnResultUtils.returnFail(400, "没有登陆！");
        }

        ChinaVo chinaVo = JSON.parseObject(redisUtils.get(USER_PRE_IP + loginUserVo.getId()).toString(), ChinaVo.class);
        int stock = cartService.queryStock(gid, chinaVo.getName());
        if (number > stock) {
            return ReturnResultUtils.returnFail(400, "牟货啦");
        }

        CartVo vo = new CartVo();
        vo.setGid(gid);
        vo.setNumber(number);
        vo.setUid(loginUserVo.getId());
        vo.setUpdateTime(new Date());
        cartService.addCart(vo);
        return ReturnResultUtils.returnSuccess(vo);

    }

    @ApiOperation("删除购物车")
    @LoginRequired
    @PostMapping(value = "/deleteCart")
    public ReturnResult deleteCart(@ApiParam("商品id") String gid, @CurrentUser LoginUserVo loginUserVo) {

        int i = cartService.deleteCart(gid);
        if (i == 1) {
            return ReturnResultUtils.returnSuccess(gid);
        }
        return ReturnResultUtils.returnFail(400, "错了");
    }


    @ApiOperation("删除IP购物车")
    @PostMapping(value = "/deleteIp")
    public ReturnResult deleteIp(@ApiParam("商品id") String gid) {

        int i = cartService.deleteIp(gid);
        if (i == 1) {
            return ReturnResultUtils.returnSuccess(gid);
        }
        return ReturnResultUtils.returnFail(400, "错了");
    }


}
