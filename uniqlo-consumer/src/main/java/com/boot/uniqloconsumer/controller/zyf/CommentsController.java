package com.boot.uniqloconsumer.controller.zyf;

import com.alibaba.dubbo.config.annotation.Reference;
import com.boot.uniqlocommon.service.CommentService;
import com.boot.uniqlocommon.service.GoodsService;
import com.boot.uniqlocommon.service.OrderService;
import com.boot.uniqlocommon.utils.ReturnResult;
import com.boot.uniqlocommon.utils.ReturnResultUtils;
import com.boot.uniqlocommon.vo.CommentsVo;
import com.boot.uniqlocommon.vo.LoginUserVo;
import com.boot.uniqlocommon.vo.OrdersVo;
import com.boot.uniqloconsumer.config.interAPI.CurrentUser;
import com.boot.uniqloconsumer.config.interAPI.LoginRequired;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by zyf 2020/7/16
 */
@Api(tags = "评论功能")
@RestController
@RequestMapping(value = "/comments")
public class CommentsController {

    @Reference
    private CommentService commentService;

    @Reference
    private GoodsService goodsService;

    @Reference
    private OrderService orderService;


    @ApiOperation("添加评论")
    @LoginRequired
    @PostMapping(value = "/add")
    public ReturnResult addComment(@Validated CommentsVo commentsVo, @CurrentUser LoginUserVo loginUserVo) {
        if (ObjectUtils.isEmpty(loginUserVo)) {
            return ReturnResultUtils.returnCodeAndMsg(601, "请登陆后进行操作");
        }
        try {
            commentsVo.setUid(loginUserVo.getId());
            List<OrdersVo> orders = orderService.getOrdersByGidAndUid(commentsVo.getGid(), loginUserVo.getId());
            if (CollectionUtils.isEmpty(orders)) {
                return ReturnResultUtils.returnCodeAndMsg(602, "尚未购买，请购买后再评价");
            }
            for (OrdersVo ordersVo : orders) {
                if (ordersVo.getIsPay() == 1) {
                    commentService.addComment(commentsVo);
                    return ReturnResultUtils.returnSuccess();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ReturnResultUtils.returnCodeAndMsg(603, "尚未支付，请支付后再评价");
    }
}
