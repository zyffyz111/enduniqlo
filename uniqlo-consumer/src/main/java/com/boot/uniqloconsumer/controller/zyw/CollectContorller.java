package com.boot.uniqloconsumer.controller.zyw;

import com.alibaba.dubbo.config.annotation.Reference;
import com.boot.uniqlocommon.service.CollectService;
import com.boot.uniqlocommon.utils.PageUtils;
import com.boot.uniqlocommon.utils.ReturnResult;
import com.boot.uniqlocommon.utils.ReturnResultUtils;
import com.boot.uniqlocommon.vo.CollectionsVo;
import com.boot.uniqlocommon.vo.GoodsVo;
import com.boot.uniqlocommon.vo.LoginUserVo;
import com.boot.uniqloconsumer.config.interAPI.CurrentUser;
import com.boot.uniqloconsumer.config.interAPI.LoginRequired;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.boot.uniqlocommon.cons.SupportOrdersEntity.*;

/**
 * Created by Charon on 2020/7/14/014
 */
@Api(tags = "收藏相关")
@RestController
@RequestMapping(value = "/collect")
public class CollectContorller {

    @Reference
    private CollectService collectService;

    @ApiOperation(value = "查看收藏列表")
    @LoginRequired
    @GetMapping(value = "/collectGoods")
    public ReturnResult<PageUtils<GoodsVo>> collectGoodsList
            (@ApiParam(hidden = true) @CurrentUser LoginUserVo loginUserVo,
             @ApiParam("当前页码") @RequestParam(defaultValue = "1") int pageNo) {
        if (ObjectUtils.isEmpty(loginUserVo)) {
            return ReturnResultUtils.returnCodeAndMsg(601, "请登陆后进行操作");
        }

        PageUtils pageUtils = new PageUtils();
        try {
            pageUtils.setCurrentList(collectService.collectGoodsList(loginUserVo.getId(), pageNo));
            pageUtils.setTotalCount(collectService.collectGoodsListTotalCount(loginUserVo.getId()));
            pageUtils.setCurrentPage(pageNo);
            return ReturnResultUtils.returnSuccess(pageUtils);
        } catch (Exception e) {
            e.printStackTrace();
            return ReturnResultUtils.returnFail(500, e.getMessage());
        }
    }

    @ApiOperation(value = "添加到收藏夹")
    @LoginRequired
    @GetMapping(value = "/addToCollection")
    public ReturnResult<String> addToCollection
            (@ApiParam(hidden = true) @CurrentUser LoginUserVo loginUserVo,
             @ApiParam("商品id") @RequestParam String gId) {
        if (ObjectUtils.isEmpty(loginUserVo)) {
            return ReturnResultUtils.returnCodeAndMsg(601, "请登陆后进行操作");
        }
        try {
            collectService.addToCollection(loginUserVo.getId(), gId);
            return ReturnResultUtils.returnSuccess(ADD_TO_COLLECTION_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return ReturnResultUtils.returnFail(505, ADD_TO_COLLECTION_FAIL);
        }
    }
}
