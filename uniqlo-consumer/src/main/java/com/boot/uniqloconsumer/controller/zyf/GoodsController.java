package com.boot.uniqloconsumer.controller.zyf;

import com.alibaba.dubbo.config.annotation.Reference;
import com.boot.uniqlocommon.service.GoodsService;
import com.boot.uniqlocommon.utils.PageUtils;
import com.boot.uniqlocommon.utils.ReturnResult;
import com.boot.uniqlocommon.utils.ReturnResultUtils;
import com.boot.uniqlocommon.vo.GoodsVo;
import com.boot.uniqlocommon.vo.TypeVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zyf 2020/7/13
 */
@Api("商品分类功能")
@RestController
@RequestMapping(value = "/goods")
public class GoodsController {

    @Reference
    private GoodsService goodsService;

    @ApiOperation("获得大分类")
    @GetMapping(value = "/topType")
    public ReturnResult<TypeVo> getTopType() {
        List<TypeVo> topTypes = new ArrayList<TypeVo>();
        try {
            topTypes = goodsService.getTopType();
        } catch (Exception e) {
            e.printStackTrace();
            return ReturnResultUtils.returnFail(100, "顶级分类错误");
        }
        return ReturnResultUtils.returnSuccess(topTypes);
    }

    @ApiOperation("获得小分类")
    @GetMapping(value = "/sonType")
    public ReturnResult<TypeVo> getTypesByParentId(@ApiParam(value = "父级id") Integer parentId) {
        List<TypeVo> typeVos = new ArrayList<TypeVo>();
        try {
            typeVos = goodsService.getTypeByParentId(parentId);
        } catch (Exception e) {
            return ReturnResultUtils.returnFail(101, "子分类错误");
        }
        return ReturnResultUtils.returnSuccess(typeVos);
    }

    @ApiOperation("根据分类id分页查询商品列表")
    @GetMapping(value = "/goodsByTypeId")
    public ReturnResult<PageUtils<GoodsVo>> getGoodsByTypeId(@ApiParam(value = "父级id") Integer typeId, @ApiParam("页码") Integer pageno) {
        PageUtils pageUtils = new PageUtils();
        try {
            pageUtils = goodsService.getGoodsByTypeId(typeId, pageno);
        } catch (Exception e) {
            return ReturnResultUtils.returnFail(103, "分类商品错误");
        }
        return ReturnResultUtils.returnSuccess(pageUtils);
    }
}
