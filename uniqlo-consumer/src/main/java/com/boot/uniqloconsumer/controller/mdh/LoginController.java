package com.boot.uniqloconsumer.controller.mdh;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.boot.uniqlocommon.service.ChinaService;
import com.boot.uniqlocommon.service.LoginService;
import com.boot.uniqlocommon.utils.*;
import com.boot.uniqlocommon.vo.ChinaVo;
import com.boot.uniqlocommon.vo.LoginUserVo;
import com.boot.uniqlocommon.vo.UsersVo;
import com.boot.uniqloconsumer.config.interAPI.CurrentUser;
import com.boot.uniqloconsumer.config.interAPI.LoginRequired;
import com.boot.uniqloconsumer.config.wxConfig.WxLoginConfig;
import com.boot.uniqloconsumer.utils.MessageUtils;
import com.boot.uniqloconsumer.utils.WeatherUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static com.boot.uniqlocommon.cons.SupportOrdersEntity.USER_PRE_IP;

/**
 * Create by maodihui on 2020/7/13
 */
@Api(tags = "登录注册")
@Log4j
@RestController
@RequestMapping(value = "/login")
public class LoginController {

    @Autowired
    private WxLoginConfig wxLoginConfig;
    @Autowired
    private MessageUtils messageUtils;
    @Autowired
    private RedisUtils redisUtils;
    @Reference
    private LoginService loginService;
    @Autowired
    private WeatherUtils weatherUtils;
    @Reference
    private ChinaService chinaService;

    @ApiOperation("发送验证码")
    @GetMapping(value = "/phone")
    public ReturnResult phoneLogin(@ApiParam("手机号码") @RequestParam("phone") String phone) {
        String code = messageUtils.send(phone);
        if (code != null) {
            redisUtils.set("user", phone);
            return ReturnResultUtils.returnSuccess();
        }
        return ReturnResultUtils.returnFail(500, "无效");
    }

    @ApiOperation("验证码注册")
    @PostMapping(value = "checkCode")
    public ReturnResult check(@ApiParam("验证码") @RequestParam("code") String code, @ApiParam("是否同意协议") @RequestParam("flag") Boolean flag) {
        if (!flag) {
            return ReturnResultUtils.returnFail(500, "请同意协议再注册！");
        }
        if (code.equals(redisUtils.get("yzm"))) {
            String phone = redisUtils.get("user").toString();
            int i = loginService.findPhone(phone);
            if (i == 1) {
                return ReturnResultUtils.returnFail(500, "手机号已存在！！");
            }
            loginService.create(phone);
            return ReturnResultUtils.returnSuccess("SUCCESS");
        }
        return ReturnResultUtils.returnFail(500, "验证码错误");
    }

    @ApiOperation("验证码登录")
    @PostMapping(value = "codeLogin")
    public ReturnResult login(@ApiParam("验证码") @RequestParam("code") String code, HttpServletRequest request) {
        if (code.equals(redisUtils.get("yzm"))) {
            String phone = redisUtils.get("user").toString();
            UsersVo vo = loginService.search(phone);
            String token = request.getSession().getId();
            String vojson = JSONObject.toJSONString(vo);
            redisUtils.set(token, vojson, 3000);

            String ip = IPUtils.getV4IP();
            Map<String, String> map = weatherUtils.getWeatherInfoByIp(ip);
            ChinaVo chinaVo = chinaService.getByName(map.get("city"));
            redisUtils.set(USER_PRE_IP + vo.getId(), JSON.toJSONString(chinaVo), 300000);

            return ReturnResultUtils.returnSuccess(token);
        }
        return ReturnResultUtils.returnFail(500, "验证码错误");

    }


    @GetMapping(value = "/getCode")
    public String getCode() {
        return wxLoginConfig.getCodeUrl();
    }

    @ApiOperation(value = "回调函数", hidden = true)
    @RequestMapping(value = "/callBack")
    public String callBack(String code) throws Exception {
        String accessTokenJsonStr = HttpClientUtils.doGet(wxLoginConfig.getAccessTokenUrl(code));
        JSONObject jsonObject = JSONObject.parseObject(accessTokenJsonStr);
        String accessToken = jsonObject.getString("access_token");
        String openid = jsonObject.getString("openid");
        String userInfo = HttpClientUtils.doGet(wxLoginConfig.getUserInfoUrl(accessToken, openid));
        log.info(userInfo);
        redisUtils.set("wx", userInfo, 500);
        return "SUCCESS";
    }


    @ApiOperation("绑定微信")
    @LoginRequired
    @GetMapping(value = "/bindWX")
    public String bindWx(@CurrentUser LoginUserVo loginUserVo) {
        String userInfo = redisUtils.get("wx").toString();
        JSONObject userInfoObj = JSON.parseObject(userInfo);
        String nickName = userInfoObj.getString("nickname");
        String openid = userInfoObj.getString("openid");
        String sex = userInfoObj.getString("sex");
        String country = userInfoObj.getString("country");
        String province = userInfoObj.getString("province");
        String city = userInfoObj.getString("city");
        String address = country + "," + province + "," + city;
        String headimg = userInfoObj.getString("headimgurl");
        int i = loginService.wxLogin(loginUserVo.getPhone(), openid, nickName, sex, address, headimg);
        if (i < 0) {
            return "FAIL";
        }
        return "success";
    }

    @ApiOperation("微信注册")
    @PostMapping(value = "/wxRegistered")
    public ReturnResult wxRegistered(Boolean flag) {
        String id = UUIDUtils.getUUIDStr(20);
        String userInfo = redisUtils.get("wx").toString();
        JSONObject userInfoObj = JSON.parseObject(userInfo);
        String nickName = userInfoObj.getString("nickname");
        String openid = userInfoObj.getString("openid");
        String sex = userInfoObj.getString("sex");
        String country = userInfoObj.getString("country");
        String province = userInfoObj.getString("province");
        String city = userInfoObj.getString("city");
        String address = country + "," + province + "," + city;
        String headimg = userInfoObj.getString("headimgurl");
        if (!flag) {
            return ReturnResultUtils.returnFail(400, "请同意注册协议");
        }
        int i = loginService.registered(id, openid, nickName, sex, address, headimg);
        if (i < 0) {
            return ReturnResultUtils.returnFail(400, "已被注册！！");
        }
        return ReturnResultUtils.returnSuccess();
    }

    @ApiOperation("微信登录")
    @PostMapping(value = "wxLogin")
    public ReturnResult wxLogin(HttpServletRequest request) {
        String userInfo = redisUtils.get("wx").toString();
        JSONObject userInfoObj = JSON.parseObject(userInfo);
        String openid = userInfoObj.getString("openid");
        UsersVo vo = loginService.search2(openid);
        if (ObjectUtils.isEmpty(vo)) {
            return ReturnResultUtils.returnFail(500, "没有该用户");
        }
        String token = request.getSession().getId();
        String vojson = JSONObject.toJSONString(vo);
        redisUtils.set(token, vojson, 5000);

        String ip = IPUtils.getV4IP();
        Map<String, String> map = weatherUtils.getWeatherInfoByIp(ip);
        ChinaVo chinaVo = chinaService.getByName(map.get("city"));
        redisUtils.set(USER_PRE_IP + vo.getId(), JSON.toJSONString(chinaVo), 3000);

        return ReturnResultUtils.returnSuccess(token);
    }

    @ApiOperation("绑定手机号")
    @LoginRequired
    @PostMapping(value = "bindPhone")
    public ReturnResult bindPhone(@CurrentUser LoginUserVo loginUserVo,
                                  @ApiParam("手机号") @RequestParam("phone") String phone,
                                  @ApiParam("验证码") @RequestParam("code") String code) {

        if (code.equals(redisUtils.get("yzm"))) {
            int i = loginService.bindPhone(loginUserVo.getOpenid(), phone);
            if (i == -1) {
                return ReturnResultUtils.returnFail(400, "已有绑定手机");
            }
            return ReturnResultUtils.returnSuccess();
        }
        return ReturnResultUtils.returnFail(400, "验证码错误！");
    }

}