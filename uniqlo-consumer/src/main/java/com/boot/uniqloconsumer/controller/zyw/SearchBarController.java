package com.boot.uniqloconsumer.controller.zyw;

import com.alibaba.dubbo.config.annotation.Reference;
import com.boot.uniqlocommon.service.HotWordsService;
import com.boot.uniqlocommon.service.SearchBarService;
import com.boot.uniqlocommon.utils.PageUtils;
import com.boot.uniqlocommon.utils.ReturnResult;
import com.boot.uniqlocommon.utils.ReturnResultUtils;
import com.boot.uniqlocommon.vo.GoodsVo;
import com.boot.uniqlocommon.vo.HotWordsVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Charon on 2020/7/14/014
 */
@Api(tags = "查询相关")
@RestController
@RequestMapping(value = "/query")
public class SearchBarController {

    @Reference
    private SearchBarService searchBarService;
    @Reference
    private HotWordsService hotWordsService;

    @ApiOperation(value = "根据关键词模糊查找")
    @GetMapping(value = "/queryGoods")
    public ReturnResult<GoodsVo> queryGoods(@ApiParam("关键词") @RequestParam String keyWords, @ApiParam("当前页码") @RequestParam(defaultValue = "1") int pageNo) {
        PageUtils pageUtils = new PageUtils();
        try {
            pageUtils.setCurrentList(searchBarService.queryGoods(keyWords, pageNo));
            pageUtils.setTotalCount(searchBarService.goodsTotalCount());
            pageUtils.setCurrentPage(pageNo);
            hotWordsService.addCount(keyWords);
            return ReturnResultUtils.returnSuccess(pageUtils);
        } catch (Exception e) {
            e.printStackTrace();
            return ReturnResultUtils.returnFail(500, e.getMessage());
        }
    }

    @ApiOperation(value = "显示热搜词")
    @PostMapping(value = "/showHotWords")
    public ReturnResult<HotWordsVo> showHotWords() {
        List<HotWordsVo> hotWordsVos = hotWordsService.hotWords();
        return ReturnResultUtils.returnSuccess(hotWordsVos);
    }
}
