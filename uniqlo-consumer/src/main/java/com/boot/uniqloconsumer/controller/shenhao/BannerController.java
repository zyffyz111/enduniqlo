package com.boot.uniqloconsumer.controller.shenhao;

import com.alibaba.dubbo.config.annotation.Reference;
import com.boot.uniqlocommon.service.BannerService;
import com.boot.uniqlocommon.utils.ReturnResult;
import com.boot.uniqlocommon.utils.ReturnResultUtils;
import com.boot.uniqlocommon.vo.BannerVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Shenhao 2020/7/13
 **/

@Api(tags = "首页轮播图")
@RestController
@RequestMapping(value = "/value=BannerController")
public class BannerController {
    @Reference
    private BannerService bannerService;

    @ApiOperation("添加轮播图")
    @GetMapping(value = "addBanner")//添加映射
    public String addBanner(@Validated BannerVo bannerVo) {
        int i = bannerService.addBanner(bannerVo);
        if (i == 1) {
            return "Seccess";
        }
        return "false";
    }

    @ApiOperation("查询轮播图")
    @GetMapping(value = "queryBanner")//添加映射
    public ReturnResult<BannerVo> queryBanner(@Validated BannerVo bannerVo) {
        List<BannerVo> bannerVos = bannerService.queryBanner();
        if (!CollectionUtils.isEmpty(bannerVos)) {
            return ReturnResultUtils.returnSuccess(bannerVos);
        }
        return ReturnResultUtils.returnFail(100, "banner error");
    }
}
