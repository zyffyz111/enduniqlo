package com.boot.uniqloconsumer.controller.shenhao;

import com.alibaba.dubbo.config.annotation.Reference;
import com.boot.uniqlocommon.param.CommentsParam;
import com.boot.uniqlocommon.service.CommentsService;
import com.boot.uniqlocommon.utils.PageUtils;
import com.boot.uniqlocommon.utils.ReturnResult;
import com.boot.uniqlocommon.utils.ReturnResultUtils;
import com.boot.uniqlocommon.vo.CommentsVo;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * Shenhao 2020/7/15
 **/
@RestController
@RequestMapping(value = "/CommentsController")
public class CommentsController {

    //调用mapper接口,定义service层；
    @Reference
    private CommentsService commentsService;

    //前台请求调用的方法
    @GetMapping(value = "addcomments")//添加映射
    public ReturnResult<CommentsVo> addcomments(@Validated CommentsParam commentsParam) {

        //判读i是否存成功
        if (null != commentsService.addcomments(commentsParam)) {
            return ReturnResultUtils.returnSuccess("评论成功");
        }
        return ReturnResultUtils.returnFail(500, "评论失败");
    }

    //请求调用的方法
    @GetMapping(value = "querycomments")//添加映射
    public ReturnResult<CommentsVo> queryComments
    (@ApiParam("商品id") @RequestParam String gId,
     @ApiParam("当前页码") @RequestParam(defaultValue = "1") int pageNo
    ) {
        //添加分页
        PageUtils pageUtils = new PageUtils();
        try {
            pageUtils.setCurrentList(commentsService.queryOrders(gId, pageNo));
            pageUtils.setTotalCount(commentsService.queryOrdersTotalCount(gId));
            pageUtils.setCurrentPage(pageNo);
            return ReturnResultUtils.returnSuccess();
        } catch (Exception e) {
            return ReturnResultUtils.returnFail(404, "页面加载失败");
        }

    }


}
