package com.boot.uniqloconsumer.controller.mdh;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.boot.uniqlocommon.service.ChinaService;
import com.boot.uniqlocommon.utils.RedisUtils;
import com.boot.uniqlocommon.utils.ReturnResult;
import com.boot.uniqlocommon.utils.ReturnResultUtils;
import com.boot.uniqlocommon.vo.ChinaVo;
import com.boot.uniqlocommon.utils.IPUtils;
import com.boot.uniqloconsumer.utils.WeatherUtils;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Map;

/**
 * Create by maodihui on 2020/7/13
 */

@Api(tags = "获取ip")
@RestController
@RequestMapping(value = "/ip")
public class IPController {

    @Autowired
    private WeatherUtils weatherUtils;
    @Reference
    private ChinaService chinaService;
    @Autowired
    private RedisUtils redisUtils;


    @GetMapping(value = "ipToAddress")
    public ReturnResult address() throws IOException {

        String ip = IPUtils.getV4IP();
        Map<String, String> map = weatherUtils.getWeatherInfoByIp(ip);
        ChinaVo vo = chinaService.getByName(map.get("city"));
        redisUtils.set(ip, JSON.toJSONString(vo));

        return ReturnResultUtils.returnSuccess(map);
    }


}
