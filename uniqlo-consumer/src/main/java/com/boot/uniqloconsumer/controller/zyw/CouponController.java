package com.boot.uniqloconsumer.controller.zyw;

import com.alibaba.dubbo.config.annotation.Reference;
import com.boot.uniqlocommon.service.CouponService;
import com.boot.uniqlocommon.utils.ReturnResult;
import com.boot.uniqlocommon.utils.ReturnResultUtils;
import com.boot.uniqlocommon.vo.CouponUserVo;
import com.boot.uniqlocommon.vo.CouponVo;
import com.boot.uniqlocommon.vo.LoginUserVo;
import com.boot.uniqloconsumer.config.interAPI.CurrentUser;
import com.boot.uniqloconsumer.config.interAPI.LoginRequired;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.boot.uniqlocommon.cons.SupportOrdersEntity.*;

/**
 * Created by Charon on 2020/7/15/015
 */
@Api(tags = "优惠券相关")
@RestController
@RequestMapping(value = "/coupon")
public class CouponController {

    @Reference
    private CouponService couponService;

    @ApiOperation(value = "买家申领优惠券")
    @LoginRequired
    @GetMapping(value = "/getCoupon")
    public ReturnResult<String> getCoupon(@ApiParam(hidden = true) @CurrentUser LoginUserVo loginUserVo,
                                          @ApiParam("优惠券id") @RequestParam String couponId) {
        if (ObjectUtils.isEmpty(loginUserVo)) {
            return ReturnResultUtils.returnCodeAndMsg(601, "请登陆后进行操作");
        }
        return ReturnResultUtils.returnSuccess(couponService.getCoupon(loginUserVo.getId(), couponId));
    }

    @ApiOperation(value = "商品优惠券列表")
    @GetMapping(value = "/CouponList")
    public ReturnResult<CouponVo> couponListByGid(@ApiParam("商品id") @RequestParam("gid") String gid) {
        List<CouponVo> couponVos = couponService.couponListByGid(gid);
        if (CollectionUtils.isEmpty(couponVos)) {
            return ReturnResultUtils.returnFail(506, "该商品没有优惠券");
        }
        return ReturnResultUtils.returnSuccess(couponVos);
    }

    /**
     * 用户未使用优惠券列表
     */
    /*@ApiOperation(value = "用户未使用优惠券列表")
    @LoginRequired
    @GetMapping(value = "/couponNotUsed")
    public ReturnResult<CouponUserVo> notUsedCouponUser(@ApiParam(hidden = true) @CurrentUser LoginUserVo loginUserVo){

    }*/
}

