package com.boot.uniqloconsumer.controller.zyf;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.boot.uniqlocommon.service.ChinaService;
import com.boot.uniqlocommon.utils.IPUtils;
import com.boot.uniqlocommon.utils.RedisUtils;
import com.boot.uniqlocommon.utils.ReturnResult;
import com.boot.uniqlocommon.utils.ReturnResultUtils;
import com.boot.uniqlocommon.vo.ChinaVo;
import com.boot.uniqlocommon.vo.LoginUserVo;
import com.boot.uniqloconsumer.config.interAPI.CurrentUser;
import com.boot.uniqloconsumer.config.interAPI.LoginRequired;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.boot.uniqlocommon.cons.SupportOrdersEntity.USER_PRE_IP;

/**
 * Created by zyf 2020/7/16
 */
@Api(tags = "省市区")
@RestController
@RequestMapping(value = "/china")
public class ChinaController {

    @Reference
    private ChinaService chinaService;

    @Autowired
    private RedisUtils redisUtils;

    @ApiOperation("获得省集合")
    @GetMapping(value = "/pro")
    public ReturnResult getProvince() {
        return ReturnResultUtils.returnSuccess(chinaService.getInfo(0));
    }

    @ApiOperation("获得市集合")
    @GetMapping(value = "/city")
    public ReturnResult getCity(@ApiParam("父级id") @RequestParam("pid") Integer pid) {
        return ReturnResultUtils.returnSuccess(chinaService.getInfo(pid));
    }

    @ApiOperation("获得区集合")
    @GetMapping(value = "/area")
    public ReturnResult getArea(@ApiParam("父级id") @RequestParam("pid") Integer pid) {
        return ReturnResultUtils.returnSuccess(chinaService.getInfo(pid));
    }

    @ApiOperation("设置购物地区")
    @LoginRequired
    @GetMapping(value = "/updateArea")
    public ReturnResult updateArea(@ApiParam("区域编码") @RequestParam("areaCode") Integer areaCode, @ApiParam(hidden = true) @CurrentUser LoginUserVo loginUserVo) {
        ChinaVo chinaVo = chinaService.getByPrimaryId(areaCode);
        redisUtils.set(USER_PRE_IP + loginUserVo.getId(), JSON.toJSONString(chinaVo));
        return ReturnResultUtils.returnSuccess();
    }
}
