package com.boot.uniqloconsumer.config.interAPI;

import com.boot.uniqlocommon.vo.LoginUserVo;
import org.springframework.core.MethodParameter;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import static org.springframework.web.context.request.RequestAttributes.SCOPE_REQUEST;

/**
 * Created by xueqijun on 2020/5/22.
 */
public class CurrentUserComplete implements HandlerMethodArgumentResolver {


    //确定是否用的是currentuser这个注解，确定是否用的是uservo指定对象
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.hasParameterAnnotation(CurrentUser.class)
                && parameter.getParameterType().isAssignableFrom(LoginUserVo.class);
    }

    //计算参数值
    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        LoginUserVo loginUserVo = (LoginUserVo)webRequest.getAttribute("loginUserVo", SCOPE_REQUEST);
        if (!ObjectUtils.isEmpty(loginUserVo)) {
            return loginUserVo;
        }
        return null;
    }
}
