package com.boot.uniqloconsumer.config.wxConfig;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by xueqijun on 2020/7/3.
 */
@Data
@Component
@ConfigurationProperties(prefix = "wx")
public class WxLoginConfig {
    private String codeUri;
    private String appid;
    private String redirectUri;
    private String responseType;
    private String scope;
    private String accessTokenUri;
    private String secret;
    private String grantType;
    private String userInfoUri;


    public String getCodeUrl() {
        StringBuffer sb = new StringBuffer(getCodeUri());
        sb.append("appid=").append(getAppid());
        sb.append("&redirect_uri=").append(getRedirectUri());
        sb.append("&response_type=").append(getResponseType());
        sb.append("&scope=").append(getScope());
        sb.append("&state=").append("STATE");
        sb.append("#wechat_redirect");
        return sb.toString();
    }

    public String getAccessTokenUrl(String code) {
        StringBuffer sb = new StringBuffer(getAccessTokenUri());
        sb.append("appid=").append(getAppid());
        sb.append("&secret=").append(getSecret());
        sb.append("&code=").append(code);
        sb.append("&grant_type=").append(getGrantType());
        return sb.toString();
    }


    public String getUserInfoUrl(String token, String openid) {
        StringBuffer sb = new StringBuffer(getUserInfoUri());
        sb.append("access_token=").append(token);
        sb.append("&openid=").append(openid);
        sb.append("&lang=zh_CN");

        return sb.toString();
    }
}
