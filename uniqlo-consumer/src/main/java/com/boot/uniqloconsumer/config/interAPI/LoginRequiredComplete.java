package com.boot.uniqloconsumer.config.interAPI;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.boot.uniqlocommon.utils.RedisUtils;
import com.boot.uniqlocommon.vo.LoginUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

/**
 * Created by xueqijun on 2020/7/2.
 */
public class LoginRequiredComplete implements HandlerInterceptor {
    @Autowired
    private RedisUtils redisUtils;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse httpServletResponse, Object handler) throws Exception {
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        LoginRequired annotation = method.getAnnotation(LoginRequired.class);
        if (null != annotation) {
            String loginToken = null;
            String wxToken = request.getHeader("wxToken");
            String token = request.getHeader("token");
            if (StringUtils.isNotEmpty(wxToken)) {
                loginToken = wxToken;
            }else if (StringUtils.isNotEmpty(token)){
                loginToken = token;
            }else {
                throw new RuntimeException("login error");
            }

            String userJsonStr = (String) redisUtils.get(loginToken);
            //什么情况这里会是空的 key过期了
            if (StringUtils.isEmpty(userJsonStr)) {
                throw new RuntimeException("请重新登陆!");
            }

            LoginUserVo loginUserVo = JSONObject.parseObject(userJsonStr, LoginUserVo.class);
            request.setAttribute("loginUserVo", loginUserVo);
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
