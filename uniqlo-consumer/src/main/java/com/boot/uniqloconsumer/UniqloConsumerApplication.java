package com.boot.uniqloconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.boot.uniqloconsumer","com.boot.uniqlocommon"})
public class UniqloConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(UniqloConsumerApplication.class, args);
    }

}
