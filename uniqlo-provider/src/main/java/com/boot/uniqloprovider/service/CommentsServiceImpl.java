package com.boot.uniqloprovider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.boot.uniqlocommon.param.CommentsParam;
import com.boot.uniqlocommon.service.CommentsService;
import com.boot.uniqlocommon.utils.PageUtils;
import com.boot.uniqlocommon.utils.UUIDUtils;
import com.boot.uniqlocommon.vo.CommentsVo;
import com.boot.uniqloprovider.dto.CommentsDto;
import com.boot.uniqloprovider.dto.CommentsExample;
import com.boot.uniqloprovider.dto.GoodsDto;
import com.boot.uniqloprovider.dto.UsersDto;
import com.boot.uniqloprovider.mapper.CommentsMapper;
import com.boot.uniqloprovider.mapper.GoodsMapper;
import com.boot.uniqloprovider.mapper.UsersMapper;
import io.swagger.annotations.Example;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Shenhao 2020/7/15
 **/
@Service
public abstract class CommentsServiceImpl implements CommentsService {
    //重写mapper方法
    @Autowired
    private CommentsMapper commentsMapper;
    @Autowired
    private GoodsMapper goodsMapper;
    @Autowired
    private UsersMapper usersMapper;


    //实现增加评论的方法
    @Override
    public Integer addcomments(CommentsParam commentsParam) {

        //new一个dto对象
        CommentsDto commentsDto = new CommentsDto();
        commentsDto.setId(UUIDUtils.getUUIDStr(20));
        commentsDto.setGid(commentsParam.getGid());
        commentsDto.setUid(commentsParam.getUid());
        commentsDto.setContent(commentsParam.getContent());
        return commentsMapper.insertSelective(commentsDto);
    }

    @Override
    public List<CommentsVo> queryOrders(String gId, int pageNo) {

        PageUtils pageUtils = new PageUtils();
        pageUtils.setPageNo(pageNo);

        CommentsExample commentsExample = new CommentsExample();
        //将数据库字段为1的筛选查询出来
        commentsExample.createCriteria().andGidEqualTo(gId);
        //设置分页条件
        commentsExample.setLimmit(pageUtils.getPageNo());
        commentsExample.setOffset(pageUtils.getPageSize());
        //把dto塞到bannerDtos集合中；
        List<CommentsDto> commentsDtos = commentsMapper.selectByExample(commentsExample);
        List<CommentsVo> commentsVos = new ArrayList<CommentsVo>();
        commentsDtos.forEach(commentsDto -> {
            CommentsVo commentsVo = new CommentsVo();
            BeanUtils.copyProperties(commentsDto, commentsVo);
            GoodsDto goodsDto = goodsMapper.selectByPrimaryKey(commentsDto.getGid());
            commentsVo.setGid(goodsDto.getId());
            UsersDto usersDto = usersMapper.selectByPrimaryKey(commentsDto.getUid());
            commentsVo.setUid(usersDto.getId());
            commentsVos.add(commentsVo);
        });
        //返回评论成功


        return commentsVos;
    }

    /**
     * 显示评论条数
     *
     * @param gid 商品id
     * @return
     */
    public long queryOrdersTotalCount(String gid) {
        CommentsExample commentsExample = new CommentsExample();
        commentsExample.createCriteria().andGidEqualTo(gid);
        return commentsMapper.countByExample(commentsExample);
    }
}