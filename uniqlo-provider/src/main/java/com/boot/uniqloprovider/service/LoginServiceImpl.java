package com.boot.uniqloprovider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.boot.uniqlocommon.service.LoginService;
import com.boot.uniqlocommon.utils.UUIDUtils;
import com.boot.uniqlocommon.vo.UsersVo;
import com.boot.uniqloprovider.dto.UsersDto;
import com.boot.uniqloprovider.dto.UsersExample;
import com.boot.uniqloprovider.mapper.UsersMapper;;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

/**
 * Create by maodihui on 2020/7/14
 */
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private UsersMapper usersMapper;

    @Override
    public Integer findPhone(String phone) {
        UsersExample usersExample = new UsersExample();
        usersExample.createCriteria().andPhoneEqualTo(phone);
        List<UsersDto> dtos = usersMapper.selectByExample(usersExample);
        if (dtos.size() > 0) {
            return 1;
        }
        return 0;
    }

    @Override
    public Integer create(String phone) {
        UsersDto dto = new UsersDto();
        dto.setPhone(phone);
        dto.setId(UUIDUtils.getUUIDStr(20));
        dto.setUpdateTime(new Date());
        int i = usersMapper.insertSelective(dto);
        return i;
    }

    @Override
    public UsersVo search(String phone) {
        UsersExample usersExample = new UsersExample();
        usersExample.createCriteria().andPhoneEqualTo(phone);
        List<UsersDto> dtos = usersMapper.selectByExample(usersExample);
        UsersVo vo = new UsersVo();
        BeanUtils.copyProperties(dtos.get(0), vo);
        return vo;
    }

    @Override
    public Integer bindPhone(String openid, String phone) {
        UsersExample usersExample = new UsersExample();
        usersExample.createCriteria().andOpenidEqualTo(openid);
        List<UsersDto> dtos = usersMapper.selectByExample(usersExample);
        UsersDto dto = dtos.get(0);
        if (dto.getPhone() != null) {
            return -1;
        }
        dto.setPhone(phone);
        int i = usersMapper.updateByPrimaryKey(dto);
        return i;
    }

    @Override
    public UsersVo search2(String openid) {
        UsersExample usersExample = new UsersExample();
        usersExample.createCriteria().andOpenidEqualTo(openid);
        List<UsersDto> dtos = usersMapper.selectByExample(usersExample);
        UsersVo vo = new UsersVo();
        BeanUtils.copyProperties(dtos.get(0), vo);
        return vo;
    }

    @Override
    public Integer wxLogin(String phone, String openid, String nickName, String sex, String address, String headimg) {

        UsersExample usersExample = new UsersExample();
        usersExample.createCriteria().andPhoneEqualTo(phone);
        List<UsersDto> dtos = usersMapper.selectByExample(usersExample);
        UsersDto dto = dtos.get(0);
        dto.setNickname(nickName);
        dto.setOpenid(openid);
        dto.setSex(Integer.parseInt(sex));
        dto.setUpdateTime(new Date());
        dto.setAddress(address);
        dto.setHeadimg(headimg);
        int i = usersMapper.updateByPrimaryKeySelective(dto);
        return i;
    }

    @Override
    public Integer registered(String id, String openid, String nickName, String sex, String address, String headimg) {
        UsersExample usersExample = new UsersExample();
        usersExample.createCriteria().andOpenidEqualTo(openid);
        List<UsersDto> dtos = usersMapper.selectByExample(usersExample);
        if (dtos.size() > 0) {
            return -1;
        }

        UsersDto dto = new UsersDto();
        dto.setId(id);
        dto.setUpdateTime(new Date());
        dto.setOpenid(openid);
        dto.setHeadimg(headimg);
        dto.setNickname(nickName);
        dto.setSex(Integer.parseInt(sex));
        dto.setAddress(address);
        int i = usersMapper.insertSelective(dto);
        return i;
    }


}
