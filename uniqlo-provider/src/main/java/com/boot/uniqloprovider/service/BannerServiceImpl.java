package com.boot.uniqloprovider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.boot.uniqlocommon.service.BannerService;
import com.boot.uniqlocommon.vo.BannerVo;
import com.boot.uniqloprovider.dto.BannerDto;
import com.boot.uniqloprovider.dto.BannerExample;
import com.boot.uniqloprovider.mapper.BannerMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Shenhao 2020/7/13
 **/
@Service
public class BannerServiceImpl implements BannerService {

    @Autowired
    private BannerMapper mapper;

    @Override
    public Integer addBanner(BannerVo bannerVo) {
        //new一个dto对象
        BannerDto bannerDto = new BannerDto();
        //复制vo到dto
        BeanUtils.copyProperties(bannerVo, bannerDto);
        //调用mapper从数据库
        return mapper.insertSelective(bannerDto);

    }

    //查询状态为1的轮播图信息
    @Override
    public List<BannerVo> queryBanner() {
        //查询返回一个集合
        List<BannerVo> bannerVos = new ArrayList<BannerVo>();
        //添加判断的条件
        BannerExample bannerExample = new BannerExample();
        //将数据库字段为1的筛选查询出来
        bannerExample.createCriteria().andStatusEqualTo(1);
        //把dto塞到bannerDtos集合中；
        List<BannerDto> bannerDtos = mapper.selectByExample(bannerExample);
        //增强for将BannerDto转换为vo
        for (BannerDto bannerDto : bannerDtos) {
            BannerVo bannerVo = new BannerVo();
            BeanUtils.copyProperties(bannerDto, bannerVo);
            bannerVos.add(bannerVo);
        }
        //返回轮播图集合
        return bannerVos;
    }
}
