package com.boot.uniqloprovider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.boot.uniqlocommon.service.CommentService;
import com.boot.uniqlocommon.utils.UUIDUtils;
import com.boot.uniqlocommon.vo.CommentsVo;
import com.boot.uniqloprovider.dto.CommentsDto;
import com.boot.uniqloprovider.mapper.CommentsMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by zyf 2020/7/16
 */
@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentsMapper commentsMapper;


    public Integer addComment(CommentsVo commentsVo) {
        commentsVo.setId(UUIDUtils.getUUIDStr(20));
        CommentsDto commentsDto = new CommentsDto();
        BeanUtils.copyProperties(commentsVo, commentsDto);
        return commentsMapper.insertSelective(commentsDto);
    }
}
