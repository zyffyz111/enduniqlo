package com.boot.uniqloprovider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.boot.uniqlocommon.service.CollectService;
import com.boot.uniqlocommon.utils.PageUtils;
import com.boot.uniqlocommon.vo.GoodsVo;
import com.boot.uniqloprovider.dto.CollectionsDto;
import com.boot.uniqloprovider.dto.CollectionsExample;
import com.boot.uniqloprovider.dto.GoodsDto;
import com.boot.uniqloprovider.mapper.CollectionsMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Charon on 2020/7/14/014
 */
@Service
public class CollectServiceImpl implements CollectService {

    @Autowired
    private CollectionsMapper collectionsMapper;

    /**
     * 展示收藏列表
     *
     * @param uId    用户id
     * @param pageNo 页码
     * @return 商品vo
     */
    @Override
    public List<GoodsVo> collectGoodsList(String uId, int pageNo) {

        PageUtils pageUtils = new PageUtils();
        pageUtils.setPageNo(pageNo);
        CollectionsExample collectionsExample = new CollectionsExample();
        collectionsExample.setLimit(pageUtils.getPageNo());
        collectionsExample.setOffset(pageUtils.getPageSize());

        List<GoodsDto> goodsDtos = collectionsMapper.selectGoodsCollection(uId);
        List<GoodsVo> goodsVos = new ArrayList<GoodsVo>();
        goodsDtos.forEach(goodsDto -> {
            GoodsVo goodsVo = new GoodsVo();
            BeanUtils.copyProperties(goodsDto, goodsVo);
            goodsVos.add(goodsVo);
        });
        return goodsVos;
    }

    /**
     * 收藏总数
     *
     * @param uId 用户id
     * @return 查询结果
     */
    @Override
    public long collectGoodsListTotalCount(String uId) {
        CollectionsExample collectionsExample = new CollectionsExample();
        collectionsExample.createCriteria().andUidEqualTo(uId);
        return collectionsMapper.countByExample(collectionsExample);
    }

    /**
     * 添加到收藏夹
     *
     * @param uId 用户id
     * @param gId 商品id
     */
    @Override
    public void addToCollection(String uId, String gId) {
        CollectionsDto collectionsDto = new CollectionsDto();
        collectionsDto.setUid(uId);
        collectionsDto.setGid(gId);
        collectionsMapper.insertSelective(collectionsDto);
    }


}
