package com.boot.uniqloprovider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.boot.uniqlocommon.service.CouponService;
import com.boot.uniqlocommon.utils.UUIDUtils;
import com.boot.uniqlocommon.vo.CouponUserVo;
import com.boot.uniqlocommon.vo.CouponVo;
import com.boot.uniqloprovider.dto.CouponDto;
import com.boot.uniqloprovider.dto.CouponExample;
import com.boot.uniqloprovider.dto.CouponUserDto;
import com.boot.uniqloprovider.dto.CouponUserExample;
import com.boot.uniqloprovider.mapper.CouponMapper;
import com.boot.uniqloprovider.mapper.CouponUserMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Charon on 2020/7/15/015
 */
@Service

public class CouponServiceImpl implements CouponService {

    @Autowired
    private CouponMapper couponMapper;

    @Autowired
    private CouponUserMapper couponUserMapper;

    /**
     * 顾客领取优惠券
     * 从coupon表中找到优惠券, 插入到coupon_user表中
     * 通过满减金额来选择领取哪个优惠券
     *
     * @param uId 用户id
     * @param id  优惠券id
     */
    @Override
    public String getCoupon(String uId, String id) {

        CouponDto couponDto = couponMapper.selectByPrimaryKey(id);
        if (ObjectUtils.isEmpty(couponDto)) {
            return null;
        }
        CouponUserDto couponUserDto = new CouponUserDto();
        couponUserDto.setId(UUIDUtils.getUUIDStr(20));
        couponUserDto.setCouponId(couponDto.getId());
        couponUserDto.setUid(uId);
        couponUserMapper.insertSelective(couponUserDto);
        return couponUserDto.getId();
    }

    @Override
    public List<CouponVo> couponListByGid(String gid) {
        List<CouponVo> couponVos = new ArrayList<CouponVo>();
        CouponExample couponExample = new CouponExample();
        couponExample.createCriteria().andGidEqualTo(gid);
        couponMapper.selectByExample(couponExample).forEach(couponDto -> {
            CouponVo couponVo = new CouponVo();
            BeanUtils.copyProperties(couponDto, couponVo);
            couponVos.add(couponVo);
        });
        return couponVos;
    }

    @Override
    public CouponVo getById(String id) {
        CouponDto couponDto = couponMapper.selectByPrimaryKey(id);
        CouponVo couponVo = new CouponVo();
        BeanUtils.copyProperties(couponDto, couponVo);
        return couponVo;
    }

    @Override
    public boolean isExistAndIsUsed(String couponUserId, String uid) {
        boolean flag = false;
        CouponUserDto couponUserDto = couponUserMapper.selectByPrimaryKey(couponUserId);
        CouponDto couponDto = couponMapper.selectByPrimaryKey(couponUserDto.getCouponId());
        if (null != couponUserDto && couponUserDto.getUid().equals(uid)
                && couponUserDto.getIsUse() == 0 && !isTimeOut(couponDto.getDays(), couponUserDto.getCreateTime())) {
            flag = true;
        }
        return flag;
    }

    @Override
    public boolean filter(double total_fee, String couponUserId) {
        boolean flag = false;
        CouponUserDto couponUserDto = couponUserMapper.selectByPrimaryKey(couponUserId);
        CouponDto couponDto = couponMapper.selectByPrimaryKey(couponUserDto.getCouponId());
        if (total_fee >= couponDto.getSatisfy() && total_fee >= couponDto.getMoney()) {
            flag = true;
        }
        return flag;
    }

    @Override
    public CouponUserVo getByCouponUserId(String couponUserId) {
        CouponUserVo couponUserVo = new CouponUserVo();
        CouponUserDto couponUserDto = couponUserMapper.selectByPrimaryKey(couponUserId);
        BeanUtils.copyProperties(couponUserDto, couponUserVo);
        return couponUserVo;
    }

    @Override
    public Integer updateCouponUserStatus(String couponUserID) {
        CouponUserDto couponUserDto = couponUserMapper.selectByPrimaryKey(couponUserID);
        couponUserDto.setIsUse(1);
        return couponUserMapper.updateByPrimaryKeySelective(couponUserDto);
    }

    private boolean isTimeOut(Integer days, Date date) {
        boolean flag = false;
        long now = System.currentTimeMillis();
        long start = date.getTime();
        if ((now - start) >= (days * 86400000)) {
            flag = true;
        }
        return flag;
    }
}
