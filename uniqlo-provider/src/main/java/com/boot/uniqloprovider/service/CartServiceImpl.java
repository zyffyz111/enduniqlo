package com.boot.uniqloprovider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.boot.uniqlocommon.service.CartService;
import com.boot.uniqlocommon.vo.CartVo;
import com.boot.uniqlocommon.vo.IpGoodsVo;
import com.boot.uniqloprovider.dto.*;
import com.boot.uniqloprovider.mapper.CartMapper;
import com.boot.uniqloprovider.mapper.IpGoodsMapper;
import com.boot.uniqloprovider.mapper.StockMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Create by maodihui on 2020/7/15
 */
@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private StockMapper stockMapper;
    @Autowired
    private IpGoodsMapper ipGoodsMapper;
    @Autowired
    private CartMapper cartMapper;


    @Override
    public Integer queryStock(String gid, String area) {
        StockExample stockExample = new StockExample();
        StockExample.Criteria criteria = stockExample.createCriteria();
        if (gid != null) {
            criteria.andGidEqualTo(gid);
        }
        if (area != null) {
            criteria.andAreaEqualTo(area);
        }

        List<StockDto> dtos = stockMapper.selectByExample(stockExample);
        if (dtos.size() == 0) {
            return -1;
        }
        int number = dtos.get(0).getNumber();
        return number;
    }

    @Override
    public Integer addIpCart(IpGoodsVo ipGoodsVo) {
        IpGoodsExample ipGoodsExample = new IpGoodsExample();
        ipGoodsExample.createCriteria().andGidEqualTo(ipGoodsVo.getGid());
        List<IpGoodsDto> dtos = ipGoodsMapper.selectByExample(ipGoodsExample);
        if (dtos.size() > 0) {
            ipGoodsVo.setNumber(ipGoodsVo.getNumber() + dtos.get(0).getNumber());
            IpGoodsDto dto = new IpGoodsDto();
            BeanUtils.copyProperties(ipGoodsVo, dto);
            return ipGoodsMapper.updateByExampleSelective(dto, ipGoodsExample);
        }

        IpGoodsDto dto = new IpGoodsDto();
        BeanUtils.copyProperties(ipGoodsVo, dto);
        int i = ipGoodsMapper.insertSelective(dto);
        return i;
    }

    @Override
    public Integer addCart(CartVo cartVo) {
        CartExample cartExample = new CartExample();
        cartExample.createCriteria().andGidEqualTo(cartVo.getGid());
        List<CartDto> dtos = cartMapper.selectByExample(cartExample);
        if (dtos.size() > 0) {
            cartVo.setNumber(cartVo.getNumber() + dtos.get(0).getNumber());
            CartDto dto = new CartDto();
            BeanUtils.copyProperties(cartVo, dto);
            return cartMapper.updateByExampleSelective(dto, cartExample);

        }

        CartDto dto = new CartDto();
        BeanUtils.copyProperties(cartVo, dto);
        int i = cartMapper.insertSelective(dto);
        return i;
    }

    @Override
    public Integer deleteIp(String gid) {
        IpGoodsExample ipGoodsExample = new IpGoodsExample();
        ipGoodsExample.createCriteria().andGidEqualTo(gid);
        int i = ipGoodsMapper.deleteByExample(ipGoodsExample);
        return i;
    }

    @Override
    public Integer deleteCart(String gid) {
        CartExample cartExample = new CartExample();
        cartExample.createCriteria().andGidEqualTo(gid);
        int i = cartMapper.deleteByExample(cartExample);
        return i;
    }

    @Override
    public List<IpGoodsVo> queryAllIp(String ip) {
        List<IpGoodsDto> dtos = ipGoodsMapper.queryAlldelete(ip);
        List<IpGoodsVo> vos = new ArrayList<IpGoodsVo>();
        dtos.forEach(dto -> {
            IpGoodsVo vo = new IpGoodsVo();
            BeanUtils.copyProperties(dto, vo);
            vos.add(vo);
        });
        IpGoodsExample ipGoodsExample = new IpGoodsExample();
        ipGoodsExample.createCriteria().andIpEqualTo(ip);
        ipGoodsMapper.deleteByExample(ipGoodsExample);
        return vos;
    }

    @Override
    public Integer insertCart(String uid, IpGoodsVo ipGoodsVo) {
        CartDto dto = new CartDto();
        dto.setGid(ipGoodsVo.getGid());
        dto.setNumber(ipGoodsVo.getNumber());
        dto.setUid(uid);
        return cartMapper.insertSelective(dto);
    }


}
