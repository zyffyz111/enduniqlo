package com.boot.uniqloprovider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.boot.uniqlocommon.service.ConsultService;
import com.boot.uniqlocommon.utils.UUIDUtils;
import com.boot.uniqlocommon.vo.ConsultVo;
import com.boot.uniqloprovider.dto.ConsultDto;
import com.boot.uniqloprovider.mapper.ConsultMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by zyf 2020/7/15
 */
@Service
public class ConsultServiceImpl implements ConsultService {

    @Autowired
    private ConsultMapper consultMapper;

    public Integer addConsult(ConsultVo consultVo) {
        try {
            String id = UUIDUtils.getUUIDStr(20);
            consultVo.setId(id);
            ConsultDto consultDto = new ConsultDto();
            BeanUtils.copyProperties(consultVo, consultDto);
            return consultMapper.insertSelective(consultDto);
        } catch (BeansException e) {
            e.printStackTrace();
        }
        return null;
    }
}
