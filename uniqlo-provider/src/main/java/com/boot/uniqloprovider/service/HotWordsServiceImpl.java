package com.boot.uniqloprovider.service;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.config.annotation.Service;
import com.boot.uniqlocommon.service.HotWordsService;
import com.boot.uniqlocommon.vo.HotWordsVo;
import com.boot.uniqloprovider.dto.HotWordsDto;
import com.boot.uniqloprovider.dto.HotWordsExample;
import com.boot.uniqloprovider.mapper.HotWordsMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Charon on 2020/7/14/014
 */
@Service
public class HotWordsServiceImpl implements HotWordsService {

    @Autowired
    private HotWordsMapper hotWordsMapper;

    /**
     * 热词展示
     *
     * @return
     */
    @Override
    public List<HotWordsVo> hotWords() {
        HotWordsExample hotWordsExample = new HotWordsExample();
        hotWordsExample.setOrderByClause("count DESC");
        hotWordsExample.createCriteria().andCountIsNotNull();
        List<HotWordsDto> hotWordsDtos = hotWordsMapper.selectByExample(hotWordsExample);
        List<HotWordsVo> hotWordsVos = new ArrayList<HotWordsVo>();
        hotWordsDtos.forEach(hotWordsDto -> {
            HotWordsVo hotWordsVo = new HotWordsVo();
            BeanUtils.copyProperties(hotWordsDto, hotWordsVo);
            hotWordsVos.add(hotWordsVo);
        });
        return hotWordsVos;
    }

    /**
     * 添加热词计数
     *
     * @param goodsName 查询名称
     * @return
     */
    @Override
    public Integer addCount(String goodsName) {
        HotWordsExample hotWordsExample = new HotWordsExample();
        hotWordsExample.createCriteria().andNameEqualTo(goodsName);
        List<HotWordsDto> hotWordsDtos = hotWordsMapper.selectByExample(hotWordsExample);
        if (CollectionUtils.isEmpty(hotWordsDtos)) {
            HotWordsDto hotWordsDto = new HotWordsDto();
            hotWordsDto.setName(goodsName);
            hotWordsDto.setLink("/query/queryGoods?goodsName=" + goodsName);
            return hotWordsMapper.insertSelective(hotWordsDto);
        }
        HotWordsDto hotWordsDto = hotWordsDtos.get(0);
        hotWordsDto.setCount(hotWordsDto.getCount() + 1);
        return hotWordsMapper.updateByExample(hotWordsDto, hotWordsExample);
    }
}
