package com.boot.uniqloprovider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.boot.uniqlocommon.service.OrderService;
import com.boot.uniqlocommon.utils.IPUtils;
import com.boot.uniqlocommon.utils.RedisUtils;
import com.boot.uniqlocommon.vo.ChinaVo;
import com.boot.uniqlocommon.vo.OrdersVo;
import com.boot.uniqloprovider.config.WeatherUtils;
import com.boot.uniqloprovider.dto.*;
import com.boot.uniqloprovider.mapper.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.boot.uniqlocommon.cons.SupportOrdersEntity.GOODS_COUNT_NAMESPACE_LOCK;
import static com.boot.uniqlocommon.cons.SupportOrdersEntity.USER_PRE_IP;

/**
 * Created by Charon on 2020/7/15/015
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private StockMapper stockMapper;
    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private CouponUserMapper couponUserMapper;
    @Autowired
    private CouponMapper couponMapper;
    @Autowired
    private ChinaMapper chinaMapper;
    @Autowired
    private WeatherUtils weatherUtils;

    /**
     * 添加未支付订单
     *
     * @param ordersVo
     * @return
     */
    @Override
    public int addNotPayOrders(OrdersVo ordersVo) {

        OrdersDto ordersDto = new OrdersDto();
        BeanUtils.copyProperties(ordersVo, ordersDto);
        return ordersMapper.insertSelective(ordersDto);
    }

    @Override
    public boolean checkNum(OrdersVo ordersVo) {

        int buyNum = ordersVo.getNumber();

        String str = redisUtils.get(USER_PRE_IP + ordersVo.getuId()).toString();
        ChinaVo chinaVo = JSON.parseObject(str, ChinaVo.class);

        StockExample stockExample = new StockExample();
        stockExample.createCriteria().andAreaEqualTo(chinaVo.getName()).andGidEqualTo(ordersVo.getgId());
        StockDto stockDto = stockMapper.selectByExample(stockExample).get(0);
        if (!isLock(ordersVo.getgId()) && buyNum <= stockDto.getNumber()) {
            return true;
        }
        return false;
    }

    /**
     * 判断是否上锁
     *
     * @param gid
     * @return
     */
    public boolean isLock(String gid) {
        return redisUtils.get(GOODS_COUNT_NAMESPACE_LOCK + gid) == null ? false : true;
    }

    @Override
    public List<OrdersVo> getOrdersByGidAndUid(String gid, String uid) {
        List<OrdersVo> ordersVos = new ArrayList<OrdersVo>();
        OrdersExample ordersExample = new OrdersExample();
        ordersExample.createCriteria().andGIdEqualTo(gid).andUIdEqualTo(uid);
        ordersMapper.selectByExample(ordersExample).forEach(ordersDto -> {
            OrdersVo ordersVo = new OrdersVo();
            BeanUtils.copyProperties(ordersDto, ordersVo);
            ordersVos.add(ordersVo);
        });
        return ordersVos;
    }
}
