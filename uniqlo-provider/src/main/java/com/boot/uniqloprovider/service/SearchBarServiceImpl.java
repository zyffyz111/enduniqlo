package com.boot.uniqloprovider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.boot.uniqlocommon.service.SearchBarService;
import com.boot.uniqlocommon.utils.PageUtils;
import com.boot.uniqlocommon.vo.GoodsVo;
import com.boot.uniqloprovider.dto.GoodsDto;
import com.boot.uniqloprovider.dto.GoodsExample;
import com.boot.uniqloprovider.mapper.CollectionsMapper;
import com.boot.uniqloprovider.mapper.GoodsMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Charon on 2020/7/14/014
 */
@Service
public class SearchBarServiceImpl implements SearchBarService {

    @Autowired
    private GoodsMapper goodsMapper;

    @Autowired
    private CollectionsMapper collectionsMapper;

    /**
     * 查询商品或种类
     *
     * @param goodsName 输入的查询名称
     * @param pageNo    页码
     * @return
     */
    @Override
    public List<GoodsVo> queryGoods(String goodsName, int pageNo) {

        //分页
        PageUtils pageUtils = new PageUtils();
        pageUtils.setPageNo(pageNo);
        //设置查找条件, 搜具体商品和类名都可以.
        List<GoodsDto> goodsDtos =
                collectionsMapper.selectGoodsAndTypeCollection(goodsName, pageUtils.getPageNo(), pageUtils.getPageSize());
        List<GoodsVo> goodsVos = new ArrayList<GoodsVo>();
        goodsDtos.forEach(goodsDto -> {
            GoodsVo goodsVo = new GoodsVo();
            BeanUtils.copyProperties(goodsDto, goodsVo);
            goodsVos.add(goodsVo);
        });
        return goodsVos;
    }

    /**
     * 查询总个数
     *
     * @return
     */
    @Override
    public long goodsTotalCount() {
        GoodsExample goodsExample = new GoodsExample();
        goodsExample.createCriteria().andStatusEqualTo(1);
        return goodsMapper.countByExample(goodsExample);
    }
}
