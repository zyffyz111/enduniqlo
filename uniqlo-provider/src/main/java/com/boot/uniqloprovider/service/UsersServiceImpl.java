package com.boot.uniqloprovider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.boot.uniqlocommon.service.UserService;
import com.boot.uniqlocommon.vo.UsersVo;
import com.boot.uniqloprovider.dto.UsersDto;
import com.boot.uniqloprovider.mapper.UsersMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Shenhao 2020/7/14
 **/
@Service
public class UsersServiceImpl implements UserService {
    @Autowired
    private UsersMapper mapper;

    //返回前端id的方法
    @Override
    public UsersVo queryUsers(String Id) {
        UsersVo usersVo = new UsersVo();
        //通过Dto查询Id；
        UsersDto usersDto = mapper.selectByPrimaryKey(Id);
        BeanUtils.copyProperties(usersDto, usersVo);
        return usersVo;
    }
}
