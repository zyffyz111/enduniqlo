package com.boot.uniqloprovider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.boot.uniqlocommon.service.GoodsService;
import com.boot.uniqlocommon.utils.IPUtils;
import com.boot.uniqlocommon.utils.PageUtils;
import com.boot.uniqlocommon.utils.RedisUtils;
import com.boot.uniqlocommon.vo.ChinaVo;
import com.boot.uniqlocommon.vo.GoodsVo;
import com.boot.uniqlocommon.vo.TypeVo;
import com.boot.uniqloprovider.config.WeatherUtils;
import com.boot.uniqloprovider.dto.*;
import com.boot.uniqloprovider.mapper.*;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.boot.uniqlocommon.cons.SupportOrdersEntity.GOODS_COUNT_NAMESPACE_LOCK;
import static com.boot.uniqlocommon.cons.SupportOrdersEntity.USER_PRE_IP;

/**
 * Created by Charon on 2020/7/15/015
 */
@Log4j
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private StockMapper stockMapper;
    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private GoodsMapper goodsMapper;

    @Autowired
    private TypeMapper typeMapper;
    @Autowired
    private WeatherUtils weatherUtils;
    @Autowired
    private CouponUserMapper couponUserMapper;


    /**
     * 减库存和更新订单
     *
     * @param orderId 订单状态
     * @return
     */
    @Override
    public boolean cutCountAndUpdateOrders(String orderId) {
        OrdersDto ordersDto = ordersMapper.selectByPrimaryKey(orderId);

        String str = redisUtils.get(USER_PRE_IP + ordersDto.getuId()).toString();
        ChinaVo chinaVo = JSON.parseObject(str, ChinaVo.class);

        StockExample stockExample = new StockExample();
        stockExample.createCriteria().andAreaEqualTo(chinaVo.getName()).andGidEqualTo(ordersDto.getgId());
        StockDto stockDto = stockMapper.selectByExample(stockExample).get(0);
        stockDto.setNumber(stockDto.getNumber() - ordersDto.getNumber());
        stockDto.setUpdateTime(new Date());
        while (true) {
            if (redisUtils.lock(GOODS_COUNT_NAMESPACE_LOCK + ordersDto.getgId(), null, 1800)) {
                stockMapper.updateByExampleSelective(stockDto, stockExample);
                redisUtils.delLock(GOODS_COUNT_NAMESPACE_LOCK + ordersDto.getgId());
                break;
            }
        }
        ordersDto.setIsPay(1);
        ordersMapper.updateByPrimaryKeySelective(ordersDto);
        //redisUtils.del(USER_PRE_IP + ordersDto.getuId());

        //改变优惠券状态
        if (null != ordersDto.getCouponuserid()) {
            CouponUserDto couponUserDto = couponUserMapper.selectByPrimaryKey(ordersDto.getCouponuserid());
            couponUserDto.setIsUse(1);
            couponUserMapper.updateByPrimaryKeySelective(couponUserDto);
        }


        return true;
    }

    /**
     * 通过商品id将dto转到vo
     *
     * @param id
     * @return
     */
    @Override
    public GoodsVo getGoodsInfoById(String id) {
        GoodsDto goodsDto = goodsMapper.selectByPrimaryKey(id);
        GoodsVo goodsVo = new GoodsVo();
        BeanUtils.copyProperties(goodsDto, goodsVo);
        return goodsVo;
    }


    /**
     * 获取顶级type集合
     *
     * @return
     */
    public List<TypeVo> getTopType() {
        List<TypeVo> typeVos = new ArrayList<TypeVo>();
        try {
            TypeExample typeExample = new TypeExample();
            typeExample.createCriteria().andParentIdEqualTo(0);
            typeMapper.selectByExample(typeExample).forEach(typeDto -> {
                TypeVo typeVo = new TypeVo();
                BeanUtils.copyProperties(typeDto, typeVo);
                typeVos.add(typeVo);
            });
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取顶级type失败", e);
        }
        return typeVos;
    }

    /**
     * 根据父类id获取子类type集合
     *
     * @param id
     * @return
     */
    public List<TypeVo> getTypeByParentId(Integer id) {
        List<TypeVo> typeVos = new ArrayList<TypeVo>();
        try {
            TypeExample typeExample = new TypeExample();
            typeExample.createCriteria().andParentIdEqualTo(id);
            typeMapper.selectByExample(typeExample).forEach(typeDto -> {
                TypeVo typeVo = new TypeVo();
                BeanUtils.copyProperties(typeDto, typeVo);
                typeVos.add(typeVo);
            });
        } catch (Exception e) {
            log.error("获取顶级type失败", e);
        }
        return typeVos;
    }

    /**
     * 根据商品id分页查询商品集合
     *
     * @param typeId
     * @param pageno
     * @return
     */
    @Override
    public PageUtils getGoodsByTypeId(Integer typeId, Integer pageno) {
        PageUtils pageUtils = new PageUtils();
        pageUtils.setPageNo(pageno);
        pageUtils.setCurrentPage(pageno);

        //按分类id查询该分类下的商品数量
        Integer totalCount = goodsMapper.countGoodsByTypeId(typeId);
        pageUtils.setTotalCount(totalCount);

        //按分类id查询该分类下的商品分页集合
        List<GoodsDto> goodsDtos = goodsMapper.getGoodsByTypeId(typeId, pageUtils.getPageNo(), pageUtils.getPageSize());
        List<GoodsVo> goodsVos = new ArrayList<GoodsVo>();
        goodsDtos.forEach(goodsDto -> {
            GoodsVo goodsVo = new GoodsVo();
            BeanUtils.copyProperties(goodsDto, goodsVo);
            goodsVos.add(goodsVo);
        });
        pageUtils.setCurrentList(goodsVos);
        return pageUtils;
    }
}
