package com.boot.uniqloprovider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.boot.uniqlocommon.service.ChinaService;
import com.boot.uniqlocommon.vo.ChinaVo;
import com.boot.uniqloprovider.dto.ChinaDto;
import com.boot.uniqloprovider.dto.ChinaExample;
import com.boot.uniqloprovider.mapper.ChinaMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Create by maodihui on 2020/7/16
 */
@Service
public class ChinaServiceImpl implements ChinaService {

    @Autowired
    private ChinaMapper chinaMapper;

    @Override
    public ChinaVo getByName(String name) {
        ChinaExample chinaExample = new ChinaExample();
        chinaExample.createCriteria().andNameLike(name + "%");
        List<ChinaDto> dtos = chinaMapper.selectByExample(chinaExample);
        ChinaVo vo = new ChinaVo();
        BeanUtils.copyProperties(dtos.get(0), vo);
        return vo;
    }

    public List<ChinaVo> getInfo(Integer pid) {
        List<ChinaVo> chinaVos = new ArrayList<ChinaVo>();
        ChinaExample chinaExample = new ChinaExample();
        chinaExample.createCriteria().andPidEqualTo(pid);
        chinaMapper.selectByExample(chinaExample).forEach(chinaDto -> {
            ChinaVo chinaVo = new ChinaVo();
            BeanUtils.copyProperties(chinaDto, chinaVo);
            chinaVos.add(chinaVo);
        });
        return chinaVos;
    }

    @Override
    public ChinaVo getByPrimaryId(Integer id) {
        ChinaDto chinaDto = chinaMapper.selectByPrimaryKey(id);
        ChinaVo chinaVo = new ChinaVo();
        BeanUtils.copyProperties(chinaDto, chinaVo);
        return chinaVo;
    }
}
