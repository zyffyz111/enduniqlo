package com.boot.uniqloprovider.dto;

import java.util.Date;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table orders
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class OrdersDto {
    /**
     * Database Column Remarks:
     *   id
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column orders.id
     *
     * @mbg.generated
     */
    private String id;

    /**
     * Database Column Remarks:
     *   商品id
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column orders.g_id
     *
     * @mbg.generated
     */
    private String gId;

    /**
     * Database Column Remarks:
     *   用户id
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column orders.u_id
     *
     * @mbg.generated
     */
    private String uId;

    /**
     * Database Column Remarks:
     *   商品数量
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column orders.number
     *
     * @mbg.generated
     */
    private Integer number;

    /**
     * Database Column Remarks:
     *   订单总价
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column orders.price
     *
     * @mbg.generated
     */
    private Double price;

    /**
     * Database Column Remarks:
     *   支付状态
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column orders.is_pay
     *
     * @mbg.generated
     */
    private Integer isPay;

    private String couponuserid;

    public String getCouponuserid() {
        return couponuserid;
    }

    public void setCouponuserid(String couponuserid) {
        this.couponuserid = couponuserid;
    }

    /**
     * Database Column Remarks:
     *   订单创建时间
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column orders.create_time
     *
     * @mbg.generated
     */
    private Date createTime;

    /**
     * Database Column Remarks:
     *   订单修改时间
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column orders.update_time
     *
     * @mbg.generated
     */
    private Date updateTime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column orders.id
     *
     * @return the value of orders.id
     *
     * @mbg.generated
     */
    public String getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column orders.id
     *
     * @param id the value for orders.id
     *
     * @mbg.generated
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column orders.g_id
     *
     * @return the value of orders.g_id
     *
     * @mbg.generated
     */
    public String getgId() {
        return gId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column orders.g_id
     *
     * @param gId the value for orders.g_id
     *
     * @mbg.generated
     */
    public void setgId(String gId) {
        this.gId = gId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column orders.u_id
     *
     * @return the value of orders.u_id
     *
     * @mbg.generated
     */
    public String getuId() {
        return uId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column orders.u_id
     *
     * @param uId the value for orders.u_id
     *
     * @mbg.generated
     */
    public void setuId(String uId) {
        this.uId = uId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column orders.number
     *
     * @return the value of orders.number
     *
     * @mbg.generated
     */
    public Integer getNumber() {
        return number;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column orders.number
     *
     * @param number the value for orders.number
     *
     * @mbg.generated
     */
    public void setNumber(Integer number) {
        this.number = number;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column orders.price
     *
     * @return the value of orders.price
     *
     * @mbg.generated
     */
    public Double getPrice() {
        return price;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column orders.price
     *
     * @param price the value for orders.price
     *
     * @mbg.generated
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column orders.is_pay
     *
     * @return the value of orders.is_pay
     *
     * @mbg.generated
     */
    public Integer getIsPay() {
        return isPay;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column orders.is_pay
     *
     * @param isPay the value for orders.is_pay
     *
     * @mbg.generated
     */
    public void setIsPay(Integer isPay) {
        this.isPay = isPay;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column orders.create_time
     *
     * @return the value of orders.create_time
     *
     * @mbg.generated
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column orders.create_time
     *
     * @param createTime the value for orders.create_time
     *
     * @mbg.generated
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column orders.update_time
     *
     * @return the value of orders.update_time
     *
     * @mbg.generated
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column orders.update_time
     *
     * @param updateTime the value for orders.update_time
     *
     * @mbg.generated
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}