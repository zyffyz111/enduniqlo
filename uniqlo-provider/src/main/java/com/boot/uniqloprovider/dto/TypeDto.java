package com.boot.uniqloprovider.dto;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table type
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class TypeDto {
    /**
     * Database Column Remarks:
     *   类别id
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column type.id
     *
     * @mbg.generated
     */
    private Integer id;

    /**
     * Database Column Remarks:
     *   类别名称
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column type.name
     *
     * @mbg.generated
     */
    private String name;

    /**
     * Database Column Remarks:
     *   父类别id
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column type.parent_id
     *
     * @mbg.generated
     */
    private Integer parentId;

    /**
     * Database Column Remarks:
     *   按类别获取集合链接
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column type.link
     *
     * @mbg.generated
     */
    private String link;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column type.id
     *
     * @return the value of type.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column type.id
     *
     * @param id the value for type.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column type.name
     *
     * @return the value of type.name
     *
     * @mbg.generated
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column type.name
     *
     * @param name the value for type.name
     *
     * @mbg.generated
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column type.parent_id
     *
     * @return the value of type.parent_id
     *
     * @mbg.generated
     */
    public Integer getParentId() {
        return parentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column type.parent_id
     *
     * @param parentId the value for type.parent_id
     *
     * @mbg.generated
     */
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column type.link
     *
     * @return the value of type.link
     *
     * @mbg.generated
     */
    public String getLink() {
        return link;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column type.link
     *
     * @param link the value for type.link
     *
     * @mbg.generated
     */
    public void setLink(String link) {
        this.link = link;
    }
}