package com.boot.uniqloprovider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.boot.uniqloprovider", "com.boot.uniqlocommon"})
public class UniqloProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(UniqloProviderApplication.class, args);
    }

}
