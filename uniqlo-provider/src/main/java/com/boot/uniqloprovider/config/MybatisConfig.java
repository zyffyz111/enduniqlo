package com.boot.uniqloprovider.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by boot on 2020/6/16.
 */
@Configuration
@MapperScan(basePackages = "com.boot.uniqloprovider.mapper")
public class MybatisConfig {
}
