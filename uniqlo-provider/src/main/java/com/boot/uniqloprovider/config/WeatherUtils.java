package com.boot.uniqloprovider.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zyf 2020/7/13
 */
@Data
@Component
@ConfigurationProperties(prefix = "weather")
public class WeatherUtils {
    private String appcode;

    private String codepath;

    private String method;

    private String host;

    private String ippath;

    public Map<String, String> getWeatherInfoByAreaCode(String code) {
        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + getAppcode());
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("areaCode", code);
        querys.put("need3HourForcast", "0");
        querys.put("needAlarm", "0");
        querys.put("needHourData", "0");
        querys.put("needIndex", "0");
        querys.put("needMoreDay", "0");
        try {

            HttpResponse response = HttpUtils.doGet(getHost(), getCodepath(), getMethod(), headers, querys);
            //获取response的body
            String str = EntityUtils.toString(response.getEntity());
            return getWeatherInfo(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Map<String, String> getWeatherInfoByIp(String ip) {
        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + getAppcode());
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("ip", ip);
        querys.put("need3HourForcast", "0");
        querys.put("needAlarm", "0");
        querys.put("needHourData", "0");
        querys.put("needIndex", "0");
        querys.put("needMoreDay", "0");
        try {
            HttpResponse response = HttpUtils.doGet(getHost(), getIppath(), method, headers, querys);
            //获取response的body
            String str = EntityUtils.toString(response.getEntity());
            return getWeatherInfo(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private Map<String, String> getWeatherInfo(String str) {
        Map<String, String> map = new HashMap<String, String>();
        JSONObject object1 = JSON.parseObject(str);
        String str2 = object1.getString("showapi_res_body");
        JSONObject object2 = JSON.parseObject(str2);
        String str3 = object2.getString("now");
        JSONObject object3 = JSON.parseObject(str3);
        String weather = object3.getString("weather");
        String temperature = object3.getString("temperature");
        String str4 = object2.getString("cityInfo");
        JSONObject object4 = JSON.parseObject(str4);
        String country = object4.getString("c9");
        String pro = object4.getString("c7");
        String city = object4.getString("c5");
        String area = object4.getString("c3");
        map.put("country", country);
        map.put("pro", pro);
        map.put("city", city);
        map.put("area", area);
        map.put("weather", weather);
        map.put("temperature", temperature);
        return map;
    }

}
